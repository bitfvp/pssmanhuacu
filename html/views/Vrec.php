<?php
$page="Recuperar senha-".$env->env_titulo;
$css="style1";
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==1){
    header("Location: {$env->env_url}a");
}
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==2){
    header("Location: {$env->env_url}aa");
}
include_once("{$env->env_root}includes/head.php");
?>
<style>
    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
    }
</style>
    <main class="container text-center form-signin font3">
        <?php
        if (isset($_SESSION['recupera_email'])){

            $sql="select * from tbl_users WHERE email=? limit 1";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam(1, $_SESSION['recupera_email']);
            $consulta->execute();
            $cadcount = $consulta->rowCount();
            $cad = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $sql="select * from tbl_recupera_senha WHERE user=? ORDER BY id desc limit 1";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam(1, $cad['id']);
            $consulta->execute();
            $reccount = $consulta->rowCount();
            $rec = $consulta->fetch();
            $sql=null;
            $consulta=null;
            $mknow = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
            if ($rec['token_time']>$mknow){
                $diftempo=$rec['token_time']-$mknow;
                if ($diftempo<3598){
                    $tempo = gmdate("H:i:s", $diftempo);
                }else{
                    $tempo = " 1 hora";
                }

            }
            ?>
            <a href="<?php echo $env->env_url;?>" class="mb-4">
                <img src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="" title="<?php echo $env->env_nome; ?>" style="max-height: 120px;"/>
            </a>
            <h1 class="h3 mb-3 fw-normal">Recuperação de senha</h1>
            <h3>Um e-mail de recuperação já foi enviado para <?php echo $_SESSION['recupera_email'];?>, verifique a caixa de entrada e spam.</h3>
            <h6 class="text-sm-center">Você fez a recuperação de senha em <?php echo datahoraBanco2data($rec['data']);?> e poderá pedir novamente após <?php echo $tempo;?>.</h6>
            <a href="<?php echo $env->env_url."index.php?pg=Vlogin"; ?>" class="btn btn-sm btn-outline-dark mt-1">FAZER LOGIN</a>
            <?php
            unset($_SESSION['recupera_email']);
        }else{
        ?>
        <form action="index.php?pg=Vrec&aca=recuperar_senha" method="post">
            <a href="<?php echo $env->env_url;?>" class="mb-4">
                <img src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="" title="<?php echo $env->env_nome; ?>" style="max-height: 120px;"/>
            </a>
            <h1 class="h3 mb-3 fw-normal">Recuperar senha</h1>
            <input autofocus type="text" name="syscpf" id="syscpf"  placeholder="CPF" autocomplete="off" class="form-control mb-1" required>
            <script>
                $(document).ready(function(){
                    $('#syscpf').mask('000.000.000-00', {reverse: false});
                });
            </script>
            <input type="email" name="sysemail" id="sysemail"  placeholder="Entre com seu E-mail cadastrado" autocomplete="off" class="form-control" required>
            <div class="checkbox mb-3">
                <br>
                <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image" style="width: 50%;" />
                <br>
                <label for="">Digite o código acima: </label>
                <input type="text" name="captcha_code" size="15" maxlength="15" autocomplete="off" required />
                <a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false"><i class="fas fa-sync text-success"></i></a>
                <br>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">RECUPERAR SENHA</button>
            <a href="<?php echo $env->env_url."index.php?pg=Vlogin"; ?>" class="btn btn-sm btn-outline-dark mt-1">OU FAZER LOGIN</a>
            <p class="text-muted">E-mail para suporte: pssmanhuacu@gmail.com</p>
        </form>
        <?php
        }
        ?>
    </main>


<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>