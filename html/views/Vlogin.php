<?php
$page="Login-".$env->env_titulo;
$css="style1";
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==1){
    header("Location: {$env->env_url}a");
}
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==2){
    header("Location: {$env->env_url}aa");
}
include_once("{$env->env_root}includes/head.php");
?>
<style>
    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
    }
</style>
    <main class="container text-center form-signin font3">
        <form action="index.php?pg=Vlogin&aca=logar" method="post">
            <a href="<?php echo $env->env_url;?>" class="mb-4">
                <img src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="" title="<?php echo $env->env_nome; ?>" style="max-height: 120px;"/>
            </a>
            <h1 class="h3 mb-3 fw-normal">Login</h1>
            <input autofocus type="text" name="syscpf" id="syscpf"  placeholder="CPF" autocomplete="off" class="form-control mb-1" required>
            <script>
                $(document).ready(function(){
                    $('#syscpf').mask('000.000.000-00', {reverse: false});
                });
            </script>
            <input type="password" name="syssenha" id="syssenha"  placeholder="Entre com sua senha" autocomplete="off" class="form-control" required>
            <div class="checkbox mb-3">
                <br>
                <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image" style="width: 50%;" />
                <br>
                <label for="">Digite o código acima: </label>
                <input type="text" name="captcha_code" size="15" maxlength="15" autocomplete="off" required />
                <a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false"><i class="fas fa-sync text-success"></i></a>
                <br>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">ENTRAR</button>
            <a href="<?php echo $env->env_url."index.php?pg=Vreg"; ?>" class="btn btn-sm btn-outline-dark mt-3">OU CADASTRE-SE</a>
            <a href="<?php echo $env->env_url."index.php?pg=Vrec"; ?>" class="btn btn-sm btn-outline-dark mt-1">OU RECUPERE A SENHA</a>
        </form>
    </main>


<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>