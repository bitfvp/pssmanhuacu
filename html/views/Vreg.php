<?php
$page="Registro-".$env->env_titulo;
$css="style1";
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==1){
    header("Location: {$env->env_url}a");
}
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==2){
    header("Location: {$env->env_url}aa");
}
include_once("{$env->env_root}includes/head.php");
?>
<style>
    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
    }
</style>
    <main class="container text-center form-signin font3">
        <form action="index.php?pg=Vreg&aca=usuarionew" method="post">
            <a href="<?php echo $env->env_url;?>" class="mb-4">
                <img src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="" title="<?php echo $env->env_nome; ?>" style="max-height: 120px;"/>
            </a>
            <h1 class="h3 mb-3 fw-normal">Cadastre-se</h1>

            <div class="row text-left">
                <div class="col-12">
                    <label for="nome">Nome Completo:</label>
                    <input autofocus type="text" name="nome" id="nome"  placeholder="Nome Completo..." class="form-control mb-1" minlength="10" min="10" autocomplete="off" required>
                </div>
            </div>
            <div class="row text-left">
                <div class="col-12">
                    <label for="cpf">CPF:</label>
                    <input type="text" name="cpf" id="cpf"  placeholder="CPF" class="form-control mb-1" minlength="14" min="14" autocomplete="off" required>
                    <script>
                        $(document).ready(function(){
                            $('#cpf').mask('000.000.000-00', {reverse: false});
                        });
                    </script>
                </div>
            </div>
            <?php
            $y18 = date("Y-m-d");
            $y18 = date("Y")-18;
            $y70 = date("Y")-70;
            ?>
            <div class="row text-left">
                <div class="col-12">
                    <label for="nascimento">Data de nascimento:</label>
                    <input type="date" name="nascimento" id="nascimento"  placeholder="" class="form-control mb-1 nodatapicker" max="<?php echo $y18."-".date("m-d");?>" min="<?php echo $y70."-".date("m-d");?>" required>
                </div>
            </div>
            <div class="row text-left">
                <div class="col-12">
                    <label for="email">E-mail:</label>
                    <input type="email" name="email" id="email"  placeholder="E-mail" class="form-control mb-1" autocomplete="off" required>
                </div>
            </div>
            <div class="row text-left">
                <div class="col-12">
                    <label for="senha1">Senha:</label>
                    <input type="password" name="senha1" id="senha1"  placeholder="Senha de 6 caracteres ou mais" class="form-control mb-1" minlength="6" min="6" autocomplete="off" required>
                </div>
            </div>
            <div class="row text-left">
                <div class="col-12">
                    <input type="password" name="senha2" id="senha2"  placeholder="Repita a senha" class="form-control" minlength="6" min="6" autocomplete="off" required>
                </div>
            </div>
            <div class="checkbox mb-3">
                <br>
                <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image" style="width: 50%;" />
                <br>
                <label for="">Digite o código acima: </label>
                <input type="text" name="captcha_code" size="15" maxlength="15" autocomplete="off" required />
                <a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false"><i class="fas fa-sync text-success"></i></a>
                <br>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">CADASTRAR</button>
            <a href="<?php echo $env->env_url."index.php?pg=Vlogin"; ?>" class="btn btn-sm btn-outline-dark mt-1">OU FAZER LOGIN</a>
        </form>
    </main>


<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>