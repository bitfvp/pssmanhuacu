<?php
class Inscricao{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncinscricao_s1_edit($id, $pessoa,
                                     $id_cargo,
                                     $nome,
                                     $naturalidade,
                                     $sexo,
                                     $cpf,
                                     $rg,
                                     $nascimento,
                                     $endereco,
                                     $bairro,
                                     $cidade,
                                     $cep,
                                     $telefone,
                                     $email,
                                     $deficiencia,
                                     $deficiencia_desc
    ){

        //inserção no banco
        try{
            $sql="UPDATE tbl_pss_inscricao SET nome=:nome, naturalidade=:naturalidade, sexo=:sexo, cpf=:cpf, "
                ."rg=:rg, nascimento=:nascimento, endereco=:endereco, bairro=:bairro, cidade=:cidade, "
                ."cep=:cep, telefone=:telefone, email=:email, deficiencia=:deficiencia, deficiencia_desc=:deficiencia_desc "
                ."WHERE id=:id and pessoa=:pessoa and id_cargo=:id_cargo";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":nome", $nome);
            $insere->bindValue(":naturalidade", $naturalidade);
            $insere->bindValue(":sexo", $sexo);
            $insere->bindValue(":cpf", $cpf);
            $insere->bindValue(":rg", $rg);
            $insere->bindValue(":nascimento", $nascimento);
            $insere->bindValue(":endereco", $endereco);
            $insere->bindValue(":bairro", $bairro);
            $insere->bindValue(":cidade", $cidade);
            $insere->bindValue(":cep", $cep);
            $insere->bindValue(":telefone", $telefone);
            $insere->bindValue(":email", $email);
            $insere->bindValue(":deficiencia", $deficiencia);
            $insere->bindValue(":deficiencia_desc", $deficiencia_desc);
            $insere->bindValue(":id", $id);
            $insere->bindValue(":pessoa", $pessoa);
            $insere->bindValue(":id_cargo", $id_cargo);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncinscricao_s2_edit($id, $pessoa,
                                         $id_cargo,
                                         $p_tempo_funcao,
                                         $p_tempo_outrafuncao,
                                         $p_habilitacao,
                                         $p_elementar,
                                         $p_medio,
                                         $p_curso,
                                         $p_periodo,
                                         $p_media_global,
                                         $p_pedagogia,
                                         $p_pos_graduacao_area,
                                         $p_mestrado,
                                         $p_normal_medio_eduinfantil,
                                         $p_normal_superior,
                                         $p_magisterio,
                                         $p_magisterio2grau
    ){

        //inserção no banco
        try{
            $sql="UPDATE tbl_pss_inscricao SET p_tempo_funcao=:p_tempo_funcao, p_tempo_outrafuncao=:p_tempo_outrafuncao, p_habilitacao=:p_habilitacao, p_elementar=:p_elementar, p_medio=:p_medio, p_curso=:p_curso, p_periodo=:p_periodo, p_media_global=:p_media_global, "
                ."p_pedagogia=:p_pedagogia, p_pos_graduacao_area=:p_pos_graduacao_area, p_mestrado=:p_mestrado, p_normal_medio_eduinfantil=:p_normal_medio_eduinfantil, p_normal_superior=:p_normal_superior, p_magisterio=:p_magisterio, p_magisterio2grau	=:p_magisterio2grau "
                ."WHERE id=:id and pessoa=:pessoa and id_cargo=:id_cargo";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":p_tempo_funcao", $p_tempo_funcao);
            $insere->bindValue(":p_tempo_outrafuncao", $p_tempo_outrafuncao);
            $insere->bindValue(":p_habilitacao", $p_habilitacao);
            $insere->bindValue(":p_elementar", $p_elementar);
            $insere->bindValue(":p_medio", $p_medio);
            $insere->bindValue(":p_curso", $p_curso);
            $insere->bindValue(":p_periodo", $p_periodo);
            $insere->bindValue(":p_media_global", $p_media_global);
            $insere->bindValue(":p_pedagogia", $p_pedagogia);
            $insere->bindValue(":p_pos_graduacao_area", $p_pos_graduacao_area);
            $insere->bindValue(":p_mestrado", $p_mestrado);
            $insere->bindValue(":p_normal_medio_eduinfantil", $p_normal_medio_eduinfantil);
            $insere->bindValue(":p_normal_superior", $p_normal_superior);
            $insere->bindValue(":p_magisterio", $p_magisterio);
            $insere->bindValue(":p_magisterio2grau", $p_magisterio2grau);
            $insere->bindValue(":id", $id);
            $insere->bindValue(":pessoa", $pessoa);
            $insere->bindValue(":id_cargo", $id_cargo);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncinscricao_s3_anexo($id, $pessoa,
                                         $id_cargo,
                                          $id_pss,
                                          $documento,
                                          $Upin
    ){
        //id é da inscricao
        //$id_cargo cargo
        //pessoa é o id de quem matricular

        //foto

        // verifica se foi enviado um arquivo
        $fillle=$_FILES['arquivo']['name'];
        if (isset($_FILES['arquivo']['name']) && $fillle[0]!=null) {//if principal
//            echo "há arquivo";

            if (is_dir("../dados/pss/". $id_pss . '/')) {} else {mkdir("../dados/pss/". $id_pss . '/');}
            if (is_dir("../dados/pss/". $id_pss . '/' . $id_cargo . '/')) {} else {mkdir("../dados/pss/". $id_pss . '/' . $id_cargo . '/');}
            if (is_dir("../dados/pss/". $id_pss . '/' . $id_cargo . '/'. $id . '/')) {} else {mkdir("../dados/pss/". $id_pss . '/' . $id_cargo . '/'. $id . '/');}

            $pasta='../dados/pss/'. $id_pss . '/' . $id_cargo . '/'. $id . '/';

            $Upin->setAllowedExtensions(array('png','jpg','jpeg','pdf'));//define as extensões permitidas para upload
            $Upin->setRandomName(true);//falamos aqui para a classe dar um nome aleatório para o nosso novo arquivo enviado
            $Upin->setMaxSize(5);//definimos o tamanho máximo que o arquivo pode ser enviado. Aqui definimos 5mb
            $Upin->setUploadDir($pasta);//definimos aqui a pasta onde os arquivos enviados ficarão ou serão enviados
            $Upin->uploadFile($_FILES['arquivo']);//chamamos a função que faz o upload do arquivo<br>


            // verifica se foi enviado um arquivo
//            $Upin->get(
//                '../dados/pss/'. $id_pss . '/' . $id_cargo . '/'. $id . '/', //Pasta de uploads (previamente criada)
//                $_FILES["arquivo"]["name"], //Pega o nome dos arquivos, altere apenas
//                15, //Tamanho máximo em MB
////                "jpg,jpeg,gif,docx,doc,xls,xlsx,pdf,png", //Extensões permitidas
//                "pdf,jpg,jpeg,png", //Extensões permitidas
//                "arquivo", //Atributo name do input file
//                1 //Mudar o nome? 1 = sim, 0 = não
//            );
//            $Upin->run();
//
//            if($Upin->res == true) {
//                $_SESSION['fsh']=[
//                    "flash"=>"info",
//                    "type"=>"success",
//                ];
//            }else{
//                $_SESSION['fsh']=[
//                    "flash"=>"info",
//                    "type"=>"danger",
//                ];
//            }

            try{
                $sql="UPDATE tbl_pss_inscricao SET tem_anexo=1 "
                    ."WHERE id=:id and pessoa=:pessoa and id_cargo=:id_cargo";

                global $pdo;
                $atuali=$pdo->prepare($sql);
                $atuali->bindValue(":id", $id);
                $atuali->bindValue(":pessoa", $pessoa);
                $atuali->bindValue(":id_cargo", $id_cargo);
                $atuali->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        }else{

            $inff = "não foi selecionado arquivo";
            $_SESSION['fsh']=[
                "flash"=>"{$inff}",
                "type"=>"warning",
            ];
        }
        //////////////////////////

        $types = array( 'png', 'jpg', 'jpeg', 'gif', 'doc', 'docx', 'pdf', 'txt', 'xls', 'xlsx' );
        if(is_dir("../dados/pss/". $id_pss . '/' . $id_cargo . '/'. $id . '/')){
//        echo "A Pasta Existe";
            if ( $handle = opendir("../dados/pss/". $id_pss . '/' . $id_cargo . '/'. $id . '/') ) {
                while ( $entry = readdir( $handle ) ) {
                    $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                    if( in_array( $ext, $types ) ){
                        $arquivoo = explode(".", $entry);//$entry é o arquivo
                        $extencao = end($arquivoo);

                        echo $arquivoo[0]."=====".$extencao;

                        $sql = "SELECT COUNT(`id`) FROM `tbl_pss_inscricao_dados` where id_inscricao='{$id}' and arquivo='{$arquivoo[0]}'";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute();
                        $total = $consulta->fetch();//$total[0]
                        $sql = null;
                        $consulta = null;
                        if($total[0]==0){
                            /////////////////////////////Fazer a inclusao
                            $sql = "INSERT INTO `tbl_pss_inscricao_dados` (`id`, `data_cadastro`, `status`,  `id_inscricao`, `documento`, `arquivo`, `extensao`)"
                                ." VALUES "
                                ."(NULL, CURRENT_TIMESTAMP, '1',     '{$id}',  '{$documento}',       '{$arquivoo[0]}',        '{$extencao}');";
                            global $pdo;
                            $update = $pdo->prepare($sql);
                            $update->execute();
                            $sql = null;
                            $update = null;
                        }else {//se for diferente é por que já esta cadastrado
//                            $inff =  "Já existe na base";
//                            $_SESSION['fsh']=[
//                                "flash"=>"{$inff}",
//                                "type"=>"warning",
//                            ];
                        }

                    }
                }
                closedir($handle);
            }
        } else {
            $inff = "<i class='text-muted'>A Pasta não Existe!</i>";
            $_SESSION['fsh']=[
                "flash"=>"{$inff}",
                "type"=>"warning",
            ];
        }


//        if(isset($consulta)){
//            /////////////////////////////////////////////////////
//            $_SESSION['fsh']=[
//                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
//                "type"=>"success",
//            ];
//
//        }else{
//            ///msg falsa forçada
//            if(empty($_SESSION['fsh'])){
//                $_SESSION['fsh']=[
//                    "flash"=>"Anexado com sucesso!!",
//                    "type"=>"success",
//                ];
//
//            }
//        }

    }





    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnc_desativa_insc($pessoa, $id_cargo){
//inserção no banco

        $sql="SELECT * from tbl_pss_inscricao "
            ."WHERE pessoa=:pessoa and id_cargo=:id_cargo limit 0,1";
        global $pdo;
        $consulta=$pdo->prepare($sql);
        $consulta->bindValue(":pessoa", $pessoa);
        $consulta->bindValue(":id_cargo", $id_cargo);
        $consulta->execute();
        $insc = $consulta->fetch();
        $sql = null;
        $consulta = null;

        $sql = "INSERT INTO `tbl_pss_inscricao_log` "
            ." (`id`, `data_ts`, `id_inscricao`, `id_pessoa`, `id_pss`, `id_cargo`, `descricao`) VALUES "
            ." (NULL, CURRENT_TIMESTAMP, :id_inscricao, :id_pessoa, :id_pss, :id_cargo, :descricao); ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":id_inscricao",$insc['id']);
        $consulta->bindValue(":id_pessoa",$insc['pessoa']);
        $consulta->bindValue(":id_pss",$insc['id_pss']);
        $consulta->bindValue(":id_cargo",$insc['id_cargo']);
        $consulta->bindValue(":descricao","desiste de cargo");
        $consulta->execute();
        $sql=null;
        $consulta=null;

        try{
            $sql="UPDATE tbl_pss_inscricao SET status=0 "
                ."WHERE pessoa=:pessoa and id_cargo=:id_cargo";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":pessoa", $pessoa);
            $insere->bindValue(":id_cargo", $id_cargo);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Inscrição desativada!!",
                "type"=>"warning",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }



    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnc_ativa_insc($pessoa, $id_cargo){
//inserção no banco

        $sql="SELECT * from tbl_pss_inscricao "
            ."WHERE pessoa=:pessoa and id_cargo=:id_cargo limit 0,1";
        global $pdo;
        $consulta=$pdo->prepare($sql);
        $consulta->bindValue(":pessoa", $pessoa);
        $consulta->bindValue(":id_cargo", $id_cargo);
        $consulta->execute();
        $insc = $consulta->fetch();
        $sql = null;
        $consulta = null;

        $sql = "INSERT INTO `tbl_pss_inscricao_log` "
            ." (`id`, `data_ts`, `id_inscricao`, `id_pessoa`, `id_pss`, `id_cargo`, `descricao`) VALUES "
            ." (NULL, CURRENT_TIMESTAMP, :id_inscricao, :id_pessoa, :id_pss, :id_cargo, :descricao); ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":id_inscricao",$insc['id']);
        $consulta->bindValue(":id_pessoa",$insc['pessoa']);
        $consulta->bindValue(":id_pss",$insc['id_pss']);
        $consulta->bindValue(":id_cargo",$insc['id_cargo']);
        $consulta->bindValue(":descricao","ativa inscricao novamente");
        $consulta->execute();
        $sql=null;
        $consulta=null;

        try{
            $sql="UPDATE tbl_pss_inscricao SET status=1 "
                ."WHERE pessoa=:pessoa and id_cargo=:id_cargo";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":pessoa", $pessoa);
            $insere->bindValue(":id_cargo", $id_cargo);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Inscrição ativada!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    //fim da class
}