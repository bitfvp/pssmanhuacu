<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark mb-2">

    <nav class='container'>
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="<?php echo $env->env_nome; ?>" style="max-height: 80px">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <a class="nav-item nav-link" href="index.php?pg=Vm">MINHAS INSCRIÇÕES</a>
<!--                <a class="nav-item nav-link" href="index.php?pg=Vpessoa_editar">DADOS PESSOAIS</a>-->
                <a class="nav-item nav-link" href="index.php?pg=Vtroca_senha">ALTERAR SENHA</a>

            </ul>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">
                        <?php
                        $primeiroNome = explode(" ", $_SESSION["nome"]);
                        echo "Olá ".$primeiroNome[0].", ".Comprimentar();
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link " href="?aca=logout" >
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <a class="navbar-brand d-none d-xl-block" data-toggle="tooltip" data-trigger="click" data-html="true" title="Grande dia! <i class='fas fa-thumbs-up text-warning'></i>">
        <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/patriaamada.png" alt="<?php echo $env->env__nome; ?>">
    </a>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    <?php
    $_SESSION['nlog']=$_SESSION['nlog']+1;
    if ($_SESSION['nlog']==1){
        ?>
        <script>
            $(function () {
                $('[data-toggle="popover"]').popover();
                $('[data-toggle="popover"]').popover('show');
            })
        </script>
        <?php
    }
    ?>
</nav>