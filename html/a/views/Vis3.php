<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==1){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
//ini_set('memory_limit', '6024M');
//ini_set("upload_max_filesize","300M");
$cr_id = get_descriptografa64($_GET['cr']);
$cr=fncgetcargo($cr_id);
$pss=fncgetpss($cr['id_pss']);
//verifica se esta ativo
//http://localhost/pssmanhuacu/html/a/index.php?pg=Vi&cr=Ng==
if ($pss['status']!=1){
    header("Location: index.php");
    exit();
}
$user=fncgetusuario($_SESSION['id']);

$sql = "SELECT * FROM\n"
    . "tbl_pss_inscricao \n"
    . "WHERE id_cargo = ? and pessoa = ? ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1,$cr_id);
$consulta->bindParam(2,$_SESSION['id']);
$consulta->execute();
$insc = $consulta->fetch();
$insccount = $consulta->rowCount();
$sql=null;
$consulta=null;
//var_dump($insccount);

if ($insccount>0){
    //exite
    //mas esta desativada
    if ($insc['status']==0){
        header("Location: index.php?pg=Vi&cr={$_GET['cr']}");
        exit();
    }
}else{
    //não existe

    if ($pss['insc_unica']==1){
        $sql = "SELECT * FROM\n"
            . "tbl_pss_inscricao \n"
            . "WHERE id_pss = ? and pessoa = ? and status = 1 ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1,$pss['id']);
        $consulta->bindParam(2,$_SESSION['id']);
        $consulta->execute();
        $insc_outra = $consulta->fetch();
        $insc_outra_count = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        if ($insc_outra_count>0){
            $_SESSION['fsh']=[
                "flash"=>"Já há uma inscrição ativa neste processo seletivo",
                "type"=>"warning",
            ];
            header("Location: index.php?pg=Vi&cr=".$_GET['cr']."/");
            exit();
        }
    }

    //inscreve
    $sql = "INSERT INTO `tbl_pss_inscricao` "
        ." (`id`, `data_ts`, `pessoa`, `id_cargo`, `id_pss`, `nome`, `naturalidade`, `sexo`, `cpf`, `rg`, "
        ." `nascimento`, `endereco`, `bairro`, `cidade`, `uf`, `cep`, `telefone`, `email`, `deficiencia`, "
        ." `deficiencia_desc`, `p_tempo_funcao`, `p_tempo_outrafuncao`, `p_habilitacao`, `p_elementar`, `p_medio`, `p_media_global`) VALUES "
        ." (NULL, CURRENT_TIMESTAMP, :pessoa, :id_cargo, :id_pss, :nome, NULL, '0', :cpf, NULL, :nacimento, "
        ." NULL, NULL, NULL, NULL, NULL, NULL, :email, '0', NULL, '0', '0', NULL, '0', '0','0'); ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":pessoa",$_SESSION['id']);
    $consulta->bindValue(":id_cargo",$cr_id);
    $consulta->bindValue(":id_pss",$pss['id']);
    $consulta->bindValue(":nome",$user['nome']);
    $consulta->bindValue(":cpf",$user['cpf']);
    $consulta->bindValue(":nacimento",$user['nascimento']);
    $consulta->bindValue(":email",$user['email']);
    $consulta->execute();
    $sql=null;
    $consulta=null;

    $sql = "SELECT * FROM\n"
        . "tbl_pss_inscricao \n"
        . "WHERE id_cargo = ? and pessoa = ? limit 0,1";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$cr_id);
    $consulta->bindParam(2,$_SESSION['id']);
    $consulta->execute();
    $insc = $consulta->fetch();
    $sql=null;
    $consulta=null;
}

$a="inscr_edit_s3_anexo";
?>
<main class="container">
    <h4 class="my-0 text-uppercase  text-center">Vaga de <?php echo $cr['nome_cargo'];?></h4>
    <h5 class="mt-0 text-uppercase  text-center">Anexos</h5>
    <div class="card mb-2">
        <div class="card-body py-0">

            <?php
            if ($cr['p_anexo']==0){?>
                <p class="text-info my-0">Essa Vaga não necessita anexar nenhum documento</p>
            <?php }else{?>

                <p class="text-danger my-0">É necessario anexar os seguintes documentos:</p>
                <p class="text-dark my-0">
                    <?php echo $cr['anexo'];?>
                </p>

                <?php
            }
            ?>
        </div>
    </div>
    <div class="d-none">
            <?php
            echo ini_get("upload_max_filesize")."<br>";
            echo ini_get("post_max_size");
            ?>
    </div>

    <?php
    if ($cr['p_anexo']==1){?>
        <form class="frmgrid" action="index.php?pg=Vis3&cr=<?php echo $_GET['cr'];?>&aca=<?php echo $a;?>" method="POST" enctype="multipart/form-data">
            <div class="card mb-2">
                <div class="card-body" style="background-color: #eeeeee;">
                    <div class="row">
                        <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $insc['id']; ?>"/>
                        <input id="id_cargo" type="hidden" class="txt bradius" name="id_cargo" value="<?php echo $insc['id_cargo']; ?>"/>
                        <input id="id_pss" type="hidden" class="txt bradius" name="id_pss" value="<?php echo $pss['id']; ?>"/>
                            <div class="col-md-12">
                                <label for="documento">Tipo de documento:</label>
                                <select name="documento" id="documento" class="form-control input-sm" required >
                                    // vamos criar a visualização

                                    <option selected="" value="0">
                                        ...Selecione o tipo de documento
                                    </option>
                                    <?php
                                    foreach (fncdocumentolist() as $item) {
                                        ?>
                                        <option value="<?php echo $item['id'];?>">
                                            <?php echo $item['documento']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <label for="arquivo">Documento para anexar: <i class="text-danger">*(Apenas pdf, jpg, jpeg ou png. Tamanho máximo 10MB)</i></label>
                                <div class="custom-file">
                                    <input id="arquivo" type="file" class="custom-file-input" name="arquivo" value=""/>
                                    <label class="custom-file-label" for="arquivo">Escolha o arquivo...</label>
                                </div>
                            </div>

                        <div class="col-md-12">
                            <input type="submit" value="ANEXAR" class="btn btn-info btn-lg btn-block mt-3" />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <?php }?>


    <?php
    if ($cr['p_anexo']==1){?>
        <div class="card mb-2">
            <div class="card-body">
                <?php
                $sql = "SELECT * FROM `tbl_pss_inscricao_dados` where id_inscricao='{$insc['id']}' and status='1' ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute();
                $dados = $consulta->fetchAll();//$total[0]
                $sql = null;
                $consulta = null;
                ?>

                <table class="table table-sm table-responsive">
                    <thead class="thead-dark">
                    <tr>
                        <th>DOCUMENTO</th>
                        <th>ARQUIVO</th>
                        <th>DATA</th>
                        <th>AÇÃO</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach ($dados as $dado){
                        echo "<tr>";

                        echo "<td>";
                        echo fncgetdocumento($dado['documento'])['documento'];
                        echo "</td>";

                        echo "<td>";
                        echo $dado['arquivo'].".".$dado['extensao'];
                        echo "</td>";

                        echo "<td>";
                        echo datahoraBanco2data($dado['data_cadastro']);
                        echo "</td>";

                        echo "<td>";
                        $link="../dados/pss/".$pss['id']."/" . $cr['id'] . "/" . $dado['id_inscricao'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                        echo "<a href='" . $link . "' target='_blank' class='btn btn-info btn-sm'>Visualizar</a>";
                        echo "</td>";


                        echo "</tr>";
                    }
                    ?>

                    </tbody>
                </table>

            </div>
        </div>
    <?php }?>

    <div class="card mb-2">
        <div class="card-body">
            <a href="index.php?pg=Vi&cr=<?php echo $_GET['cr'];?>" class="btn btn-success btn-lg btn-block mt-3">FINALIZAR</a>
        </div>
    </div>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>