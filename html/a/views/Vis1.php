<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==1){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

$cr_id = get_descriptografa64($_GET['cr']);
$cr=fncgetcargo($cr_id);
$pss=fncgetpss($cr['id_pss']);
//verifica se esta ativo
//http://localhost/pssmanhuacu/html/a/index.php?pg=Vi&cr=Ng==
if ($pss['status']!=1){
    header("Location: index.php");
    exit();
}
$user=fncgetusuario($_SESSION['id']);

$sql = "SELECT * FROM\n"
    . "tbl_pss_inscricao \n"
    . "WHERE id_cargo = ? and pessoa = ? ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1,$cr_id);
$consulta->bindParam(2,$_SESSION['id']);
$consulta->execute();
$insc = $consulta->fetch();
$insccount = $consulta->rowCount();
$sql=null;
$consulta=null;
//var_dump($insccount);

if ($insccount>0){
    //exite
    //mas esta desativada
    if ($insc['status']==0){
        header("Location: index.php?pg=Vi&cr={$_GET['cr']}");
        exit();
    }
}else{
    //não existe

    if ($pss['insc_unica']==1){
        $sql = "SELECT * FROM\n"
            . "tbl_pss_inscricao \n"
            . "WHERE id_pss = ? and pessoa = ? and status = 1 ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1,$pss['id']);
        $consulta->bindParam(2,$_SESSION['id']);
        $consulta->execute();
        $insc_outra = $consulta->fetch();
        $insc_outra_count = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        if ($insc_outra_count>0){
            $_SESSION['fsh']=[
                "flash"=>"Já há uma inscrição ativa neste processo seletivo",
                "type"=>"warning",
            ];
            header("Location: index.php?pg=Vi&cr={$_GET['cr']}");
            exit();
        }
    }

    //inscreve
    $sql = "INSERT INTO `tbl_pss_inscricao` "
        ." (`id`, `data_ts`, `pessoa`, `id_cargo`, `id_pss`, `nome`, `naturalidade`, `sexo`, `cpf`, `rg`, "
        ." `nascimento`, `endereco`, `bairro`, `cidade`, `uf`, `cep`, `telefone`, `email`, `deficiencia`, "
        ." `deficiencia_desc`, `p_tempo_funcao`, `p_tempo_outrafuncao`, `p_habilitacao`, `p_elementar`, `p_medio`, `p_media_global`) VALUES "
        ." (NULL, CURRENT_TIMESTAMP, :pessoa, :id_cargo, :id_pss, :nome, NULL, '0', :cpf, NULL, :nacimento, "
        ." NULL, NULL, NULL, NULL, NULL, NULL, :email, '0', NULL, '0', '0', NULL, '0', '0','0'); ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":pessoa",$_SESSION['id']);
    $consulta->bindValue(":id_cargo",$cr_id);
    $consulta->bindValue(":id_pss",$pss['id']);
    $consulta->bindValue(":nome",$user['nome']);
    $consulta->bindValue(":cpf",$user['cpf']);
    $consulta->bindValue(":nacimento",$user['nascimento']);
    $consulta->bindValue(":email",$user['email']);
    $consulta->execute();
    $sql=null;
    $consulta=null;

    $sql = "SELECT * FROM\n"
        . "tbl_pss_inscricao \n"
        . "WHERE id_cargo = ? and pessoa = ? limit 0,1";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$cr_id);
    $consulta->bindParam(2,$_SESSION['id']);
    $consulta->execute();
    $insc = $consulta->fetch();
    $sql=null;
    $consulta=null;
}

$a="inscr_edit_s1";
?>
<main class="container">
    <h4 class="my-0 text-uppercase  text-center">Vaga de <?php echo $cr['nome_cargo'];?></h4>
    <h5 class="mt-0 text-uppercase  text-center">Dados Pessoais</h5>

    <form class="frmgrid" action="index.php?pg=Vis1&cr=<?php echo $_GET['cr'];?>&aca=<?php echo $a;?>" method="post">
        <div class="card mb-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <label  class="large" for="nome">Nome:</label>
                        <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $insc['id']; ?>"/>
                        <input id="id_cargo" type="hidden" class="txt bradius" name="id_cargo" value="<?php echo $insc['id_cargo']; ?>"/>
                        <input autocomplete="off" autofocus id="nome" type="text" class="form-control" name="nome" placeholder="Preencha com seu nome completo" value="<?php echo $insc['nome']; ?>" maxlength="200" required />
                    </div>
                    <div class="col-md-12">
                        <label  class="large" for="naturalidade">Naturalidade:</label>
                        <input autocomplete="off" autofocus id="naturalidade" type="text" class="form-control" name="naturalidade" placeholder="Local de nascimento"  value="<?php echo $insc['naturalidade']; ?>" maxlength="200"  />
                    </div>

                    <div class="col-md-12">
                        <label for="sexo">Sexo:</label>
                        <select name="sexo" id="sexo" class="form-control">// vamos criar a visualização de sexo
                            <option selected="" value="<?php if ($insc['sexo'] == "") {
                                $z = 0;
                                echo $z;
                            } else {
                                echo $insc['sexo'];
                            } ?>">
                                <?php
                                if ($insc['sexo'] == 0) {
                                    echo "Selecione...";
                                }
                                if ($insc['sexo'] == 1) {
                                    echo "Feminino";
                                }
                                if ($insc['sexo'] == 2) {
                                    echo "Masculino";
                                }
                                if ($insc['sexo'] == 3) {
                                    echo "Não declarado";
                                }
                                ?>
                            </option>
                            <option value="1">Feminino</option>
                            <option value="2">Masculino</option>
                            <option value="3">Não declarado</option>
                        </select>
                    </div>

                    <div class="col-md-12">
                        <label  class="large" for="">CPF:</label>
                        <input name="cpf" id="cpf" type="text" class="form-control" value="<?php echo $insc['cpf']; ?>" required  />
                        <script>
                            $(document).ready(function(){
                                $('#cpf').mask('000.000.000-00', {reverse: false});
                            });
                        </script>
                    </div>

                    <div class="col-md-12">
                        <label  class="large" for="rg">RG:</label>
                        <input name="rg" id="rg" type="text" class="form-control" placeholder="Número da carteira de identidade" value="<?php echo $insc['rg']; ?>" maxlength="50" />
                    </div>
                    <?php
                    $y18 = date("Y-m-d");
                    $y18 = date("Y")-18;
                    $y70 = date("Y")-70;
                    ?>
                    <div class="col-md-12">
                        <label  class="large" for="">Nascimento:</label>
                        <input name="nascimento" id="nascimento" type="date" class="form-control nodatapicker" value="<?php echo $insc['nascimento']; ?>" max="<?php echo $y18."-".date("m-d");?>" min="<?php echo $y70."-".date("m-d");?>" required />
                    </div>

                    <div class="col-md-12">
                        <label for="endereco">Endereço:</label>
                        <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" placeholder="Nome da rua e número de onde reside"  value="<?php echo $insc['endereco']; ?>" maxlength="200"  />
                    </div>

                    <div class="col-md-12">
                        <label for="bairro">Bairro:</label>
                        <input autocomplete="off" id="bairro" type="text" class="form-control" name="bairro" placeholder="Bairro onde reside" value="<?php echo $insc['bairro']; ?>" maxlength="100" />
                    </div>

                    <div class="col-md-12">
                        <label for="cidade">Cidade:</label>
                        <input autocomplete="off" id="cidade" type="text" class="form-control" name="cidade" placeholder="Cidade onde reside" value="<?php echo $insc['cidade']; ?>" maxlength="100"  />
                    </div>

                    <div class="col-md-12">
                        <label for="cep">CEP:</label>
                        <input autocomplete="off" id="cep" type="text" class="form-control" name="cep" value="<?php echo $insc['cep']; ?>"/>
                        <script>
                            $(document).ready(function(){
                                $('#cep').mask('00000-000', {reverse: false});
                            });
                        </script>
                    </div>

                    <div class="col-md-12">
                        <label for="telefone">Telefone:</label>
                        <input autocomplete="off" id="telefone" type="text" class="form-control" name="telefone" value="<?php echo $insc['telefone']; ?>" maxlength="100" />
                    </div>

                    <div class="col-md-12">
                        <label for="email">E-mail:</label>
                        <input autocomplete="off" id="email" type="text" class="form-control" name="email" value="<?php echo $insc['email']; ?>" maxlength="150" />
                    </div>

                    <?php
                    if ($cr['p_deficiencia']==1){?>

                        <div class="col-md-12">
                            <label for="deficiencia">Possui alguma deficiência:</label>
                            <select name="deficiencia" id="deficiencia" class="form-control">
                                // vamos criar a visualização de rf
                                <option selected="" value="<?php if ($insc['deficiencia'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['deficiencia'];
                                } ?>">
                                    <?php
                                    if ($insc['deficiencia'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['deficiencia'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>


                        <div class="col-md-12">
                            <label for="deficiencia_desc">Especificar Deficiência(CID):</label>
                            <input autocomplete="off" id="deficiencia_desc" type="text" class="form-control" name="deficiencia_desc" value="<?php echo $insc['deficiencia_desc']; ?>"  />
                        </div>

                    <?php }
                    ?>

                    <div class="col-md-12">
                        <input type="submit" value="SALVAR E CONTINUAR >>" class="btn btn-success btn-lg btn-block mt-3" />
                    </div>

                </div>
            </div>
        </div>



    </form>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>