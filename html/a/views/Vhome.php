<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==1){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
$sorte=rand(1,10);
?>
<main class="container">
    <?php
    $sql = "SELECT * FROM\n"
        . "tbl_pss \n"
        . "WHERE status <> 0 ORDER BY \n"
        . "status, data_ts DESC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    //    $consulta->bindParam(1,$ps['id']);
    $consulta->execute();
    $processo = $consulta->fetchAll();
    $sql=null;
    $consulta=null;

    $order=0;
    foreach ($processo as $ps){
        if ($ps['status']==1){
            $psstatus="INSCRIÇÕES ABERTAS";
            $pscor="success";
        }
        if ($ps['status']==2){
            $psstatus="INSCRIÇÕES ENCERRADAS";
            $pscor="danger";
        }
        ?>
    <section id="part2" class="my-3" >
        <div class="container">
            <h1 class="font3 pt-3 pb-0 text-uppercase">
                <strong class="badge badge-<?php echo $pscor?>"><?php echo $psstatus?></strong><br>
                <?php echo $ps['nome_ps'];?> - <?php echo $ps['cod_ps'];?>
            </h1>
            <h4 class="font3 text-uppercase"><?php echo $ps['secretaria'];?></h4>
            <h5 class="font2">
                <p class="font2 my-0 pb-2">
                    <?php echo $ps['descricao'];?>
                </p>
                <p class="font2 my-0">
                    Período de inscrição de <?php echo dataBanco2data($ps['periodo_insc_i']);?> a <?php echo dataBanco2data($ps['periodo_insc_f']);?>
                </p>
                <strong class="font2 my-0">
                    Link para o edital completo <a href="<?php echo $ps['edital'];?>" target="_blank" class="btn btn-sm btn-outline-info">CLICK AQUI</a>
                </strong>
                <?php
                if ($ps['insc_unica']==1){
                    echo "<p class='font2 my-0 text-info'>Esse processo seletivo só permite a inscrição em um cargo.</p>";
                }
                ?>
                <p class="font2 my-0">
                    A Classificação Final será publicada no site <?php echo $ps['local_resultado'];?> da Prefeitura Municipal de Manhuaçu, no dia  <?php echo dataBanco2data($ps['data_resultado']);?>.
                    Consulte o calendário completo do processo seletivo no edital.
                </p>
            </h5>
            <hr width="30%" align="left" class="hr-grosso">
        </div>
    </section>

    <section id="part3">
        <?php
        if ($ps['status']==1){

        $sql = "SELECT * FROM\n"
            . "tbl_pss_cargo \n"
            . "WHERE id_pss = ? and status = 1 ORDER BY \n"
            . "nome_cargo ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1,$ps['id']);
        $consulta->execute();
        $cargos = $consulta->fetchAll();
        $cargoscont = $consulta->rowCount();
        $sql=null;
        $consulta=null;

        foreach ($cargos as $cr){
            $order=1;

            if ($order % 2 == 0){
                $ordem_a = " order-md-12 ";
                $ordem_b = " order-md-1 ";
            }else{
                $ordem_a = "  ";
                $ordem_b = "  ";
            }
            ?>

            <div class="container">
                <div class="row">
                    <div class=" col-xs-12 col-sm-12 col-md-9 col-lg-9  <?php echo $ordem_a;?> ">
                        <div class="holder-para">
                            <h3 class="font3 text-uppercase">CARGO DE <?php echo $cr['nome_cargo'];?></h3>
                            <hr class="hr-short">
                            <p class="font2 my-0">
                                Atribuição: <?php echo $cr['atribuicao'];?>
                            </p>
                            <p class="font2 my-0">
                                Quantidade de vagas: <?php echo $cr['vagas'];?>
                            </p>
                            <p class="font2 my-0">
                                Remuneração: <?php echo $cr['remuneracao'];?>
                            </p>
                            <p class="font2 my-0">
                                Jornada semanal: <?php echo $cr['jornada_semanal'];?>
                            </p>
                            <p class="font2 my-0">
                                Local de lotação: <?php echo $cr['local_lotacao'];?>
                            </p>
                            <p class="font2 my-0">
                                Requisito da vaga:<br> <?php echo $cr['pre_requisito'];?>
                            </p>
                            <?php

                            $sql = "SELECT * FROM\n"
                                . "tbl_pss_inscricao \n"
                                . "WHERE id_cargo = ? and pessoa = ? ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1,$cr['id']);
                            $consulta->bindParam(2,$_SESSION['id']);
                            $consulta->execute();
                            $insc = $consulta->fetch();
                            $insc_count = $consulta->rowCount();
                            $sql=null;
                            $consulta=null;
                            //                                var_dump($insc);

                            $cr_id = get_criptografa64($cr['id']);
                            if ($insc_count>0){
                                if ($insc['status']==1){
                                    echo "Você já está cadastrado, <a class='btn btn-sm btn-info m-2' href='index.php?pg=Vi&cr={$cr_id}'>EDITAR INSCRIÇÃO</a> ou <a class='btn btn-sm btn-info m-2' href='index.php?pg=Vc&cr={$cr_id}' target='_blank'>EMITIR COMPROVANTE</a>";
                                }else{
                                    echo "Você Desistiu desse cargo, <a class='btn btn-sm btn-info m-2' href='index.php?pg=Vi&cr={$cr_id}'>ACESSE</a>";
                                }
                            }else{
                                echo "Para se inscrever no cargo <a class='btn btn-sm btn-outline-info' href='index.php?pg=Vi&cr={$cr_id}'>CLICK AQUI</a>";
                            }
                            ?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 <?php echo $ordem_b;?> ">
                        <div class="holder-img">
                            <img class="round-img img-responsive center-block" src="<?php echo $cr['imagem'];?>">
                        </div>
                    </div>
                </div>

                <hr width="90%" class="hr-grosso">
            </div>


        <?php }//foreach
        }//if
        ?>

    </section>

            <?php } ?>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>