<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==1){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page = "Minhas inscrições-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


            // Recebe
            $id_user = $_SESSION['id'];
            //existe um id e se ele é numérico
            if (!empty($id_user) && is_numeric($id_user)) {
                // Captura os dados do cliente solicitado
                $sql = "SELECT * \n"
                    . "FROM tbl_pss_inscricao \n"
                    . "WHERE pessoa=? \n"
                    . "order by data_ts desc LIMIT 0,1000";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $id_user);
                $consulta->execute();
                $mat = $consulta->fetchAll();
                $contar = $consulta->rowCount();
                $sql = null;
                $consulta = null;
            }
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Minhas inscrições
                </div>
                <div class="card-body">
                    <h6>
                        <table class="table table-sm table-responsive">
                            <thead>
                                <tr>
                                    <th>PSS</th>
                                    <th>SECRETARIA</th>
                                    <th>CARGO</th>
                                    <th>COMPROVANTE</th>
                                    <th>DATA</th>
                                    <th>MAIS</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($mat as $at) {
                                $cr=fncgetcargo($at['id_cargo']);
                                $cr_encrip=get_criptografa64($cr['id']);
                                $ps=fncgetpss($cr['id_pss']);


                                if ($at['status']==1){
                                    $status="<i class='text-success'>ATIVO</i>";
                                }
                                ?>
                                <tr>
                                    <td><?php echo $ps['nome_ps'];?>-<?php echo $ps['cod_ps'];?></td>
                                    <td><?php echo $ps['secretaria'];?></td>
                                    <td><?php echo $cr['nome_cargo'];?></td>
                                    <?php
                                    if ($at['status']==0){
                                        echo "<td><i class='text-danger'>DESISTIU</i></td>";
                                    }
                                    if ($at['status']==1){
                                        echo "<td><a class='btn btn-sm btn-outline-info' href='index.php?pg=Vc&cr={$cr_encrip}' target='_blank'>ACESSAR COMPROVANTE DA INSCRIÇÃO</a></td>";
                                    }
                                    ?>
                                    <td><?php echo datahoraBanco2data($at['data_ts']);?></td>
                                    <td>
                                        <?php
                                        if ($ps['status']==1){
                                            echo "<a href='index.php?pg=Vi&cr={$cr_encrip}' class='btn btn-outline-info btn-sm'>ACESSAR</a>";
                                        }else{
                                            echo "<i class='text-danger'>ENCERRADO</i>";
                                        }
                                        ?>

                                    </td>
                                </tr>

                            <?php } ?>

                            </tbody>
                        </table>
                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>