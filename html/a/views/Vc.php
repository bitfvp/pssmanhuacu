<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==1){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
// Recebe

$id_cargo= get_descriptografa64($_GET['cr']);
$cr=fncgetcargo($id_cargo);
$ps=fncgetpss($cr['id_pss']);
$sql = "SELECT * \n"
    . "FROM tbl_pss_inscricao \n"
    . "WHERE pessoa=? and id_cargo=? ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_SESSION['id']);
    $consulta->bindParam(2, $id_cargo);
    $consulta->execute();
    $insc = $consulta->fetch();
    $sql=null;
    $consulta=null;
?>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h2 class="text-left ml-4 my-0"><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h2>
            <h6 class="text-left ml-4 my-0">Lei Provincial n°. 2407 de 5/XI/1877 - Área: 628.43 km² - Altitude: 612 metros</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</h6>
            <h4 class="my-0"><strong><?php echo $ps['secretaria'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo $ps['nome_ps'];?> <?php echo $ps['cod_ps'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo $cr['nome_cargo'];?></strong></h4>
            <h5 class="my-0"><strong><?php echo $ps['referencia'];?></strong></h5>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-8 offset-2">
            <h4 class="float-right"> DATA DA INSCRIÇÃO: <?php echo datahoraBanco2data($insc['data_ts']);?></h4>
            <h4> CÓDIGO: <?php $id64=$insc['id']+2021; echo get_criptografa64($id64);?></h4>
            <h3> NOME COMPLETO: <strong><?php echo $insc['nome'];?></strong></h3>
            <h4> CPF: <?php echo mask($insc['cpf'],'###.###.###-##');?></h4>
            <h4> NASCIMENTO: <?php echo dataBanco2data($insc['nascimento']);?></h4>
            <h4> ENDEREÇO: <?php echo $insc['endereco'];?> - <?php echo $insc['bairro'];?> - <?php echo $insc['cidade'];?> - <?php echo $insc['cep'];?></h4>

            <h4> TELEFONE: <?php echo $insc['telefone'];?></h4>
            <h4> E-MAIL: <?php echo $insc['email'];?></h4>
            <h5 class="text-center mt-4"><strong>INFORMAÇÕES RETIRADAS DO https://pssmanhuacu.com.br/</strong></h5>
            <h5 class="text-center"><strong>ÀS <?php echo date("H:i:s");?> DE <?php echo date("d/m/Y");?></strong></h5>
        </div>
    </div>
    <br>
</main>

</body>
</html>