<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==1){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

$cr_id = get_descriptografa64($_GET['cr']);
$cr=fncgetcargo($cr_id);
$pss=fncgetpss($cr['id_pss']);
//verifica se esta ativo
if ($pss['status']!=1){
    header("Location: index.php");
    exit();
}
$user=fncgetusuario($_SESSION['id']);

$sql = "SELECT * FROM\n"
    . "tbl_pss_inscricao \n"
    . "WHERE id_cargo = ? and pessoa = ? ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1,$cr_id);
$consulta->bindParam(2,$_SESSION['id']);
$consulta->execute();
$insc = $consulta->fetch();
$insccount = $consulta->rowCount();
$sql=null;
$consulta=null;
//var_dump($insccount);

if ($insccount>0){
    //exite
    //mas esta desativada
    if ($insc['status']==0){
        header("Location: index.php?pg=Vi&cr={$_GET['cr']}");
        exit();
    }
}else{
    //não existe

    if ($pss['insc_unica']==1){
        $sql = "SELECT * FROM\n"
            . "tbl_pss_inscricao \n"
            . "WHERE id_pss = ? and pessoa = ? and status = 1 ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1,$pss['id']);
        $consulta->bindParam(2,$_SESSION['id']);
        $consulta->execute();
        $insc_outra = $consulta->fetch();
        $insc_outra_count = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        if ($insc_outra_count>0){
            $_SESSION['fsh']=[
                "flash"=>"Já há uma inscrição ativa neste processo seletivo",
                "type"=>"warning",
            ];
            header("Location: index.php?pg=Vi&cr=".$_GET['cr']."/");
            exit();
        }
    }

    //inscreve
    $sql = "INSERT INTO `tbl_pss_inscricao` "
        ." (`id`, `data_ts`, `pessoa`, `id_cargo`, `id_pss`, `nome`, `naturalidade`, `sexo`, `cpf`, `rg`, "
        ." `nascimento`, `endereco`, `bairro`, `cidade`, `uf`, `cep`, `telefone`, `email`, `deficiencia`, "
        ." `deficiencia_desc`, `p_tempo_funcao`, `p_tempo_outrafuncao`, `p_habilitacao`, `p_elementar`, `p_medio`, `p_media_global`) VALUES "
        ." (NULL, CURRENT_TIMESTAMP, :pessoa, :id_cargo, :id_pss, :nome, NULL, '0', :cpf, NULL, :nacimento, "
        ." NULL, NULL, NULL, NULL, NULL, NULL, :email, '0', NULL, '0', '0', NULL, '0', '0','0'); ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":pessoa",$_SESSION['id']);
    $consulta->bindValue(":id_cargo",$cr_id);
    $consulta->bindValue(":id_pss",$pss['id']);
    $consulta->bindValue(":nome",$user['nome']);
    $consulta->bindValue(":cpf",$user['cpf']);
    $consulta->bindValue(":nacimento",$user['nascimento']);
    $consulta->bindValue(":email",$user['email']);
    $consulta->execute();
    $sql=null;
    $consulta=null;

    $sql = "SELECT * FROM\n"
        . "tbl_pss_inscricao \n"
        . "WHERE id_cargo = ? and pessoa = ? limit 0,1";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$cr_id);
    $consulta->bindParam(2,$_SESSION['id']);
    $consulta->execute();
    $insc = $consulta->fetch();
    $sql=null;
    $consulta=null;
}

$a="inscr_edit_s2";
?>
<main class="container">
    <h4 class="my-0 text-uppercase  text-center">Vaga de <?php echo $cr['nome_cargo'];?></h4>
    <h5 class="mt-0 text-uppercase  text-center">Relativo à experiência, formação e outros</h5>

    <form class="frmgrid" action="index.php?pg=Vis2&cr=<?php echo $_GET['cr'];?>&aca=<?php echo $a;?>" method="post">
        <div class="card mb-2">
            <div class="card-body">
                <div class="row">
                    <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $insc['id']; ?>"/>
                    <input id="id_cargo" type="hidden" class="txt bradius" name="id_cargo" value="<?php echo $insc['id_cargo']; ?>"/>

                    <?php
                    if ($cr['p_tempo_funcao']==1){?>
                        <div class="col-md-12">
                            <label for="p_tempo_funcao">Preencha o tempo em dias nessa função: <i class="text-warning">*1 ponto a cada ano, máximo 5 pontos</i></label>
                            <input autocomplete="off" id="p_tempo_funcao" type="text" class="form-control" name="p_tempo_funcao" value="<?php echo $insc['p_tempo_funcao']; ?>" required  />
                            <script>
                                $(document).ready(function(){
                                    $('#p_tempo_funcao').mask('000000', {reverse: false});
                                });
                            </script>
                        </div>
                    <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_tempo_outrafuncao']==1){?>
                        <div class="col-md-12">
                            <label for="p_tempo_outrafuncao">Preencha o tempo em dias, em outra função na Prefeitura Municipal de Manhuaçu:*</label>
                            <input autocomplete="off" id="p_tempo_outrafuncao" type="text" class="form-control" name="p_tempo_outrafuncao" value="<?php echo $insc['p_tempo_outrafuncao']; ?>" required />
                            <script>
                                $(document).ready(function(){
                                    $('#p_tempo_outrafuncao').mask('000000', {reverse: false});
                                });
                            </script>
                        </div>
                        <?php
                    }
                    ?>


                    <?php
                    if ($cr['p_habilitacao']==1){?>
                        <div class="col-md-12">
                            <label for="p_habilitacao">Preencha sua Habilitação, Licenciatura ou Graduação:*</label>
                            <input autocomplete="off" id="p_habilitacao" type="text" class="form-control" name="p_habilitacao" placeholder="Preencha com sua formação" value="<?php echo $insc['p_habilitacao']; ?>" required />
                        </div>
                        <?php
                    }
                    ?>


                    <?php
                    ////////////////////
                    if ($cr['p_elementar']==1){?>
                        <div class="col-md-12">
                            <label for="p_elementar">Possui ensino elementar completo:</label>
                            <select name="p_elementar" id="p_elementar" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_elementar'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_elementar'];
                                } ?>">
                                    <?php
                                    if ($insc['p_elementar'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_elementar'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }

                    if ($cr['p_medio']==1){?>
                        <div class="col-md-12">
                            <label for="p_medio">Possui ensino médio completo:</label>
                            <select name="p_medio" id="p_medio" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_medio'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_medio'];
                                } ?>">
                                    <?php
                                    if ($insc['p_medio'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_medio'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>


                    <?php
                    if ($cr['p_curso']==1){?>
                        <div class="col-md-12">
                            <label for="p_curso">Preencha seu curso em andamento:*</label>
                            <input autocomplete="off" id="p_curso" type="text" class="form-control" name="p_curso" placeholder="Preencha com seu curso" value="<?php echo $insc['p_curso']; ?>" required />
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_periodo']==1){?>
                        <div class="col-md-12">
                            <label for="p_periodo">Período atual:</label>
                            <select name="p_periodo" id="p_periodo" class="form-control" required>
                                <option selected="" value="<?php if ($insc['p_periodo'] == "") {
                                    echo "";
                                } else {
                                    echo $insc['p_periodo'];
                                } ?>">
                                    <?php
                                    if ($insc['p_periodo'] == 0) {
                                        echo "Escolha...";
                                    }
                                    if ($insc['p_periodo'] == 1) {
                                        echo "1º Período";
                                    }
                                    if ($insc['p_periodo'] == 2) {
                                        echo "2º Período";
                                    }
                                    if ($insc['p_periodo'] == 3) {
                                        echo "3º Período";
                                    }
                                    if ($insc['p_periodo'] == 4) {
                                        echo "4º Período";
                                    }
                                    if ($insc['p_periodo'] == 5) {
                                        echo "5º Período";
                                    }
                                    if ($insc['p_periodo'] == 6) {
                                        echo "6º Período";
                                    }
                                    if ($insc['p_periodo'] == 7) {
                                        echo "7º Período";
                                    }
                                    if ($insc['p_periodo'] == 8) {
                                        echo "8º Período";
                                    }
                                    if ($insc['p_periodo'] == 9) {
                                        echo "9º Período";
                                    }
                                    if ($insc['p_periodo'] == 10) {
                                        echo "10º Período";
                                    }
                                    ?>
                                </option>
                                <option value="1">1º Período</option>
                                <option value="2">2º Período</option>
                                <option value="3">3º Período</option>
                                <option value="4">4º Período</option>
                                <option value="5">5º Período</option>
                                <option value="6">6º Período</option>
                                <option value="7">7º Período</option>
                                <option value="8">8º Período</option>
                                <option value="9">9º Período</option>
                                <option value="10">10º Período</option>
                            </select>
                        </div>

                        <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_media_global']==1){?>
                        <div class="col-md-12">
                            <label for="p_media_global">Média global das notas do curso <i>de acordo com o histórico acadêmico fornecido pela sua instituição de ensino:</i><br><strong class="fas fa-exclamation-circle" title="Fornecido pela instituição de ensino"></strong><i class="text-danger"> Para valores fracionados usar vírgula</i></label>
                            <input autocomplete="off" id="p_media_global" type="number" class="form-control" name="p_media_global" value="<?php echo $insc['p_media_global']; ?>" step="0.01" required />
                        </div>
                        <?php
                    }
                    ?>


<!--                    ////////////////////////////////////////-->
                    <?php
                    if ($cr['p_pedagogia']==1){?>
                        <div class="col-md-12">
                            <label for="p_pedagogia">Possui formação em pedagogia: <i class="text-warning">*10 pontos de valor</i></label>
                            <select name="p_pedagogia" id="p_pedagogia" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_pedagogia'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_pedagogia'];
                                } ?>">
                                    <?php
                                    if ($insc['p_pedagogia'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_pedagogia'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_pos_graduacao_area']==1){?>
                        <div class="col-md-12">
                            <label for="p_pos_graduacao_area">Possui pós graduação na área: <i class="text-warning">*15 pontos de valor</i></label>
                            <select name="p_pos_graduacao_area" id="p_pos_graduacao_area" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_pos_graduacao_area'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_pos_graduacao_area'];
                                } ?>">
                                    <?php
                                    if ($insc['p_pos_graduacao_area'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_pos_graduacao_area'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_mestrado']==1){?>
                        <div class="col-md-12">
                            <label for="p_mestrado">Possui mestrado: <i class="text-warning">*20 pontos de valor</i></label>
                            <select name="p_mestrado" id="p_mestrado" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_mestrado'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_mestrado'];
                                } ?>">
                                    <?php
                                    if ($insc['p_mestrado'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_mestrado'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_normal_medio_eduinfantil']==1){?>
                        <div class="col-md-12">
                            <label for="p_normal_medio_eduinfantil">Possui normal de nível médio (específico para educação infantil):</label>
                            <select name="p_normal_medio_eduinfantil" id="p_normal_medio_eduinfantil" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_normal_superior'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_normal_medio_eduinfantil'];
                                } ?>">
                                    <?php
                                    if ($insc['p_normal_medio_eduinfantil'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_normal_medio_eduinfantil'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_normal_superior']==1){?>
                        <div class="col-md-12">
                            <label for="p_normal_superior">Possui normal superior: <i class="text-warning">*5 pontos de valor</i></label>
                            <select name="p_normal_superior" id="p_normal_superior" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_normal_superior'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_normal_superior'];
                                } ?>">
                                    <?php
                                    if ($insc['p_normal_superior'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_normal_superior'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_magisterio']==1){?>
                        <div class="col-md-12">
                            <label for="p_magisterio">Possui magistério: </label>
                            <select name="p_magisterio" id="p_magisterio" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_magisterio'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_magisterio'];
                                } ?>">
                                    <?php
                                    if ($insc['p_magisterio'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_magisterio'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if ($cr['p_magisterio2grau']==1){?>
                        <div class="col-md-12">
                            <label for="p_magisterio2grau">Possui magistério a nível de 2° grau: </label>
                            <select name="p_magisterio2grau" id="p_magisterio2grau" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="<?php if ($insc['p_magisterio2grau'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $insc['p_magisterio2grau'];
                                } ?>">
                                    <?php
                                    if ($insc['p_magisterio2grau'] == 0) {
                                        echo "Não";
                                    }
                                    if ($insc['p_magisterio2grau'] == 1) {
                                        echo "Sim";
                                    } ?>
                                </option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <?php
                    }
                    ?>


<!--                    ////////////////////////////////////////-->




                    <div class="col-md-12">
                        <input type="submit" value="SALVAR E CONTINUAR >>" class="btn btn-success btn-lg btn-block mt-3" />
                    </div>
                </div>
            </div>
        </div>


    </form>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>