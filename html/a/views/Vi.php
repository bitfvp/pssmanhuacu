<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==1){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

$cr_id = get_descriptografa64($_GET['cr']);
$cr=fncgetcargo($cr_id);
$pss=fncgetpss($cr['id_pss']);
//verifica se esta ativo
//http://localhost/pssmanhuacu/html/a/index.php?pg=Vi&cr=Ng==
if ($cr['status']==0 or $pss['status']!=1){
    header("Location: index.php");
    exit();
}
$user=fncgetusuario($_SESSION['id']);

$sql = "SELECT * FROM\n"
    . "tbl_pss_inscricao \n"
    . "WHERE id_cargo = ? and pessoa = ? ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1,$cr_id);
$consulta->bindParam(2,$_SESSION['id']);
$consulta->execute();
$insc = $consulta->fetch();
$insccount = $consulta->rowCount();
$sql=null;
$consulta=null;

if ($pss['insc_unica']==1){
    $sql = "SELECT * FROM\n"
        . "tbl_pss_inscricao \n"
        . "WHERE id_pss = ? and pessoa = ? and status = 1 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$pss['id']);
    $consulta->bindParam(2,$_SESSION['id']);
    $consulta->execute();
    $outra_insc = $consulta->fetch();
    $outra_insccount = $consulta->rowCount();
    $sql=null;
    $consulta=null;
}else{
    $outra_insccount = 0;
}


?>
<main class="container">
<h4 class="mb-0 text-uppercase text-center">Formulário de inscrição</h4>
    <h5 class="my-0 text-uppercase  text-center">Vaga de <?php echo $cr['nome_cargo'];?></h5>
    <h6 class="mt-0 text-uppercase  text-center">Processo Seletivo <?php echo $pss['cod_ps'];?> da <?php echo $pss['secretaria'];?></h6>
    <?php
    if ($outra_insccount==0){
        if ($insccount>0 and $insc['status']==1){
            //exite e esta ativa
            ?>
            <div class="card mb-2">
                <div class="card-body">
                    <p class="text-center text-info my-0">Parabéns <?php echo $insc['nome']; ?>, você já tem seu cadastro nesta vaga.</p>
                    <p class="text-center text-dark my-0"> Este processo seletivo chegará ao fim das inscrições no dia <?php echo dataBanco2data($pss['periodo_insc_f']);?>.</p>
                    <p class="text-center text-dark my-0"> Acompanhe o resultado no site <?php echo $pss['local_resultado'];?> no dia <?php echo dataBanco2data($pss['data_resultado']);?>.</p>
                    <p class="text-center text-dark my-0"> Para alguma alteração na inscrição, acesse abaixo:</p>
                    <a href='index.php?pg=Vis1&cr=<?php echo $_GET['cr']; ?>' class="btn btn-outline-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Altere seus dados pessoais</a>
                    <a href='index.php?pg=Vis2&cr=<?php echo $_GET['cr']; ?>' class="btn btn-outline-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Altere seus dados de experiência</a>
                    <a href='index.php?pg=Vis3&cr=<?php echo $_GET['cr']; ?>' class="btn btn-outline-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Altere seus anexos</a>
                    <a href='index.php?pg=Vc&cr=<?php echo $_GET['cr']; ?>' target='_blank' class="btn btn-info btn-lg btn-block mb-1"><i class="fas fa-arrow-right"></i> Veja seu comprovante de Inscrição</a>
                    <i class="text-info mt-1"> *Para melhor visualização e impressão do comprovante de inscrição, acesse utilizando um computador.</i>
                    <hr>
                    <a href='index.php?pg=Vi&cr=<?php echo $_GET['cr']; ?>&aca=desativa_insc' class="btn btn-danger btn-lg btn-block"><i class="fas fa-arrow-right"></i> Desistir dessa inscrição</a>
                    <i class="text-danger mt-1"> *Só é possível se inscrever em outro cargo deste processo seletivo desistindo dessa inscrição.</i>
                </div>
            </div>
            <?php
        }
        if ($insccount>0 and $insc['status']==0){
            //exite e esta desativa
            ?>
            <div class="card mb-2">
                <div class="card-body">
                    <p class="text-center text-info my-0">Olá <?php echo $insc['nome']; ?>, você já tem seu cadastro nesta vaga, porém foi desativado por você</p>
                    <p class="text-center text-dark my-0"> Este processo seletivo chegará ao fim das inscrições no dia <?php echo dataBanco2data($pss['periodo_insc_f']);?>.</p>
                    <a href='index.php?pg=Vi&cr=<?php echo $_GET['cr']; ?>&aca=ativa_insc' class="btn btn-outline-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Reativar inscrição</a>
                </div>
            </div>
            <?php
        }

        if ($insccount==0){
            ?>
            <div class="card mb-2">
                <div class="card-body">
                    <p class="text-center text-danger">Para concluir sua inscrição nessa vaga, preencha os campos nas próximas telas com seus dados e envie os documentos que forem necessários.</p>
                    <?php
                    if ($cr['p_anexo']==0){?>
                        <p class="text-info my-0 text-center d-none">Essa Vaga não necessita anexar nenhum documento</p>
                    <?php }else{?>

                        <p class="text-danger my-0 text-center">Esse cargo necessita do anexo dos seguintes documentos:</p>
                        <p class="text-dark my-0 text-center">
                            <?php echo $cr['anexo'];?>
                        </p>

                        <?php
                    }
                    ?>
                    <a href='index.php?pg=Vis1&cr=<?php echo $_GET['cr']; ?>' class="btn btn-outline-success btn-lg btn-block mt-1">Preencher sua inscrição</a>

                </div>
            </div>
            <?php
        }

    }

    if ($outra_insccount!=0){
        if ($insccount>0 and $insc['status']==1){
            //exite e esta ativa
            ?>
            <div class="card mb-2">
                <div class="card-body">
                    <p class="text-center text-info my-0">Parabéns <?php echo $insc['nome']; ?>, você já tem seu cadastro nesta vaga.</p>
                    <p class="text-center text-dark my-0"> Este processo seletivo chegará ao fim das inscrições no dia <?php echo dataBanco2data($pss['periodo_insc_f']);?>.</p>
                    <p class="text-center text-dark my-0"> Acompanhe o resultado no site <?php echo $pss['local_resultado'];?> no dia <?php echo dataBanco2data($pss['data_resultado']);?>.</p>
                    <p class="text-center text-dark my-0"> Para alguma alteração na inscrição, acesse abaixo:</p>
                    <a href='index.php?pg=Vis1&cr=<?php echo $_GET['cr']; ?>' class="btn btn-outline-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Altere seus dados pessoais</a>
                    <a href='index.php?pg=Vis2&cr=<?php echo $_GET['cr']; ?>' class="btn btn-outline-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Altere seus dados de experiência</a>
                    <a href='index.php?pg=Vis3&cr=<?php echo $_GET['cr']; ?>' class="btn btn-outline-success btn-lg btn-block"><i class="fas fa-arrow-right"></i> Altere seus anexos</a>
                    <a href='index.php?pg=Vc&cr=<?php echo $_GET['cr']; ?>' target='_blank' class="btn btn-info btn-lg btn-block mb-1"><i class="fas fa-arrow-right"></i> Veja seu comprovante de Inscrição</a>
                    <i class="text-info mt-1"> *Para melhor visualização e impressão do comprovante de inscrição, acesse utilizando um computador.</i>
                    <hr>
                    <a href='index.php?pg=Vi&cr=<?php echo $_GET['cr']; ?>&aca=desativa_insc' class="btn btn-danger btn-lg btn-block"><i class="fas fa-arrow-right"></i> Desistir dessa inscrição</a>
                </div>
            </div>
            <?php
        }
        if ($insccount>0 and $insc['status']==0){
            //exite e esta desativa
            ?>
            <div class="card mb-2">
                <div class="card-body">
                    <p class="text-center text-info my-0">Olá <?php echo $insc['nome']; ?>, você já tem seu cadastro nesta vaga, porém foi desativado por você</p>
                    <p class="text-center text-dark my-0"> Este processo seletivo chegará ao fim das inscrições no dia <?php echo dataBanco2data($pss['periodo_insc_f']);?>.</p>
                    <h2 class="text-danger"> <i class="fas fa-exclamation-triangle text-warning"></i>Foi encontrado uma inscrição sua <strong>ativa</strong> em um outro cargo neste processo seletivo, para que você possa reativar a inscrição aqui terá que desistir da outra inscrição.</h2>
                </div>
            </div>
            <?php
        }

        if ($insccount==0){
            ?>
            <div class="card mb-2">
                <div class="card-body">
                    <p class="text-center text-info my-0">Olá <?php echo $insc['nome']; ?>, você ainda não tem cadastro nesta vaga</p>
                    <p class="text-center text-dark my-0"> Este processo seletivo chegará ao fim das inscrições no dia <?php echo dataBanco2data($pss['periodo_insc_f']);?>.</p>
                    <h2 class="text-danger"> <i class="fas fa-exclamation-triangle text-warning"></i> Foi encontrado uma inscrição sua <strong>ativa</strong> em um outro cargo neste processo seletivo, para que você possa se inscrever nesse cargo você terá que desistir da outra inscrição.</h2>

                </div>
            </div>
            <?php
        }
    }
    ?>


</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>