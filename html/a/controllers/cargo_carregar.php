<?php
function fnccargolist(){
    $sql = "SELECT * FROM tbl_pss_cargo where status=1 ORDER BY data_ts desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $cargolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $cargolista;
}

function fncgetcargo($id){
    $sql = "SELECT * FROM tbl_pss_cargo WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getcargo = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcargo;
}