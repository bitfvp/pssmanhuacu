<?php
function fncpsslist(){
    $sql = "SELECT * FROM tbl_pss where status=1 ORDER BY data_ts desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $pslista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pslista;
}

function fncgetpss($id){
    $sql = "SELECT * FROM tbl_pss WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getps = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getps;
}