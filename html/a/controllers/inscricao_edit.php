<?php
if($startactiona==1 && $aca=="inscr_edit_s1"){
    $pessoa=$_SESSION["id"];
    $id = $_POST["id"];
    $id_cargo = $_POST["id_cargo"];
    $id_cargo2 = get_descriptografa64($_GET["cr"]);
    $nome = remover_caracter(ucwords(strtolower($_POST["nome"])));
    $naturalidade = remover_caracter(ucwords(strtolower($_POST["naturalidade"])));
    $sexo = $_POST["sexo"];
    $cpf = limpadocumento($_POST["cpf"]);
    $rg = limpadocumento($_POST["rg"]);
    $nascimento = $_POST["nascimento"];
    $endereco = remover_caracter(ucwords(strtolower($_POST["endereco"])));
    $bairro = remover_caracter(ucwords(strtolower($_POST["bairro"])));
    $cidade = remover_caracter(ucwords(strtolower($_POST["cidade"])));
    $cep = limpadocumento($_POST["cep"]);
    $telefone = $_POST["telefone"];
    $email = $_POST["email"];
    if (isset($_POST['deficiencia'])){
        $deficiencia = $_POST['deficiencia'];
    }else{
        $deficiencia = 0;
    }
    if (isset($_POST['deficiencia_desc'])){
        $deficiencia_desc = $_POST['deficiencia_desc'];
    }else{
        $deficiencia_desc = null;
    }

    //
    if($id_cargo2!=$id_cargo){
        $_SESSION['fsh']=[
            "flash"=>"Houve um erro",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vhome");
        exit();
    }else {
        //executa classe
        $salvar= new Inscricao();
        $salvar->fncinscricao_s1_edit(
            $id,
            $pessoa,
            $id_cargo,
            $nome,
            $naturalidade,
            $sexo,
            $cpf,
            $rg,
            $nascimento,
            $endereco,
            $bairro,
            $cidade,
            $cep,
            $telefone,
            $email,
            $deficiencia,
            $deficiencia_desc
        );
        header("Location: index.php?pg=Vis2&cr={$_GET["cr"]}");
        exit();
    }

}


//////////////////////////////////////////////////
if($startactiona==1 && $aca=="inscr_edit_s2"){
    $pessoa=$_SESSION["id"];
    $id = $_POST["id"];
    $id_cargo = $_POST["id_cargo"];
    $id_cargo2 = get_descriptografa64($_GET["cr"]);

    if (isset($_POST["p_tempo_funcao"])){
        $p_tempo_funcao = $_POST['p_tempo_funcao'];
    }else{
        $p_tempo_funcao = 0;
    }
    if (isset($_POST["p_tempo_outrafuncao"])){
        $p_tempo_outrafuncao = $_POST['p_tempo_outrafuncao'];
    }else{
        $p_tempo_outrafuncao = 0;
    }
    if (isset($_POST["p_habilitacao"])){
        $p_habilitacao = remover_caracter(ucwords(strtolower($_POST['p_habilitacao'])));
    }else{
        $p_habilitacao = null;
    }

    if (isset($_POST["p_elementar"])){
        $p_elementar = $_POST['p_elementar'];
    }else{
        $p_elementar = 0;
    }

    if (isset($_POST["p_medio"])){
        $p_medio = $_POST['p_medio'];
    }else{
        $p_medio = 0;
    }
    if ($p_medio==1){
        $p_elementar = 1;
    }


    if (isset($_POST["p_curso"])){
        $p_curso = remover_caracter(ucwords(strtolower($_POST['p_curso'])));
    }else{
        $p_curso = null;
    }
    if (isset($_POST["p_periodo"])){
        $p_periodo = remover_caracter(ucwords(strtolower($_POST['p_periodo'])));
    }else{
        $p_periodo = null;
    }
    if (isset($_POST["p_media_global"])){
        $p_media_global = $_POST['p_media_global'];
    }else{
        $p_media_global = 0;
    }

    //
    if (isset($_POST["p_pedagogia"])){
        $p_pedagogia = $_POST['p_pedagogia'];
    }else{
        $p_pedagogia = 0;
    }
    if (isset($_POST["p_pos_graduacao_area"])){
        $p_pos_graduacao_area = $_POST['p_pos_graduacao_area'];
    }else{
        $p_pos_graduacao_area = 0;
    }
    if (isset($_POST["p_mestrado"])){
        $p_mestrado = $_POST['p_mestrado'];
    }else{
        $p_mestrado = 0;
    }
    if (isset($_POST["p_normal_medio_eduinfantil"])){
        $p_normal_medio_eduinfantil = $_POST['p_normal_medio_eduinfantil'];
    }else{
        $p_normal_medio_eduinfantil = 0;
    }
    if (isset($_POST["p_normal_superior"])){
        $p_normal_superior = $_POST['p_normal_superior'];
    }else{
        $p_normal_superior = 0;
    }
    if (isset($_POST["p_magisterio"])){
        $p_magisterio = $_POST['p_magisterio'];
    }else{
        $p_magisterio = 0;
    }
    if (isset($_POST["p_magisterio2grau"])){
        $p_magisterio2grau = $_POST['p_magisterio2grau'];
    }else{
        $p_magisterio2grau = 0;
    }
    //


    //
    if($id_cargo2!=$id_cargo){
        $_SESSION['fsh']=[
            "flash"=>"Houve um erro",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vhome");
        exit();
    }else {
        //executa classe
        $salvar= new Inscricao();
        $salvar->fncinscricao_s2_edit(
            $id,
            $pessoa,
            $id_cargo,
            $p_tempo_funcao,
            $p_tempo_outrafuncao,
            $p_habilitacao,
            $p_elementar,
            $p_medio,
            $p_curso,
            $p_periodo,
            $p_media_global,
            $p_pedagogia,
            $p_pos_graduacao_area,
            $p_mestrado,
            $p_normal_medio_eduinfantil,
            $p_normal_superior,
            $p_magisterio,
            $p_magisterio2grau
        );
        header("Location: index.php?pg=Vis3&cr={$_GET["cr"]}");
        exit();
    }

}


//////////////////////////////////////////////////
if($startactiona==1 && $aca=="inscr_edit_s3_anexo"){
    $pessoa=$_SESSION["id"];
    $id = $_POST["id"];
    $id_cargo = $_POST["id_cargo"];
    $id_cargo2 = get_descriptografa64($_GET["cr"]);
    $id_pss = $_POST["id_pss"];
    $documento = $_POST['documento'];



    //
    if($id_cargo2!=$id_cargo){
        $_SESSION['fsh']=[
            "flash"=>"Houve um erro",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vhome");
        exit();

//        echo $id_cargo2."----------".$id_cargo."+++++++<br>";
//        var_dump($_POST);
    }else {

        require_once("{$env->env_root}models/Upin.class.php");
//        $Upin = new Upin;
        $Upin = new Upload();

        //executa classe
        $salvar= new Inscricao();
        $salvar->fncinscricao_s3_anexo(
            $id,
            $pessoa,
            $id_cargo,
            $id_pss,
            $documento,
            $Upin
        );
        header("Location: index.php?pg=Vis3&cr={$_GET["cr"]}");
        exit();
    }

}

//////////////////////////////////////////////////
if($startactiona==1 && $aca=="desativa_insc"){
    $pessoa=$_SESSION["id"];
    $id_cargo = get_descriptografa64($_GET["cr"]);

    //
    if(!isset($id_cargo)){
        $_SESSION['fsh']=[
            "flash"=>"Houve um erro",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vhome");
        exit();


    }else {

        //executa classe
        $salvar= new Inscricao();
        $salvar->fnc_desativa_insc($pessoa, $id_cargo);
        header("Location: index.php?pg=Vi&cr={$_GET["cr"]}");
        exit();
    }

}

//////////////////////////////////////////////////
if($startactiona==1 && $aca=="ativa_insc"){
    $pessoa=$_SESSION["id"];
    $id_cargo = get_descriptografa64($_GET["cr"]);


    //
    if(!isset($id_cargo)){
        $_SESSION['fsh']=[
            "flash"=>"Houve um erro",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vhome");
        exit();


    }else {

        //executa classe
        $salvar= new Inscricao();
        $salvar->fnc_ativa_insc($pessoa, $id_cargo);
        header("Location: index.php?pg=Vi&cr={$_GET["cr"]}");
        exit();
    }

}


