<?php
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==2){
    //acesso liberado
}else{
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();;
}

$page="Processo seletivo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $ps=fncgetpss($_GET['id']);
    if ($ps['profissional']==$_SESSION['id'] or $_SESSION['id']==1){
        //de boa
    }else{
        header("Location: index.php?pg=Vpss_lista");
        exit();;
    }
}else{
    echo "HOUVE ALGUM ERRO";
}
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados do processo seletivo
                </div>
                <div class="card-body">
                <blockquote class="blockquote blockquote-info">
                    <header>CÓDIGO: <strong class="text-info"><?php echo $ps['cod_ps']; ?>&nbsp;&nbsp;</strong></header>
                    <h6>
                        PROCESSO SELETIVO: <strong class="text-info"><?php echo $ps['nome_ps']; ?></strong>
                    </h6>
                    <h6>
                        DESCRIÇÃO: <strong class="text-info"><?php echo $ps['descricao']; ?></strong>
                    </h6>
                    <h6>
                        SECRETARIA: <strong class="text-info"><?php echo $ps['secretaria']; ?></strong>
                    </h6>
                    <h6>
                        DATA DE CADASTRO: <strong class="text-info"><?php echo datahoraBanco2data($ps['data_ts']); ?></strong>
                    </h6>

                    <h6>
                        INÍCIO DAS INSCRIÇÕES: <strong class="text-info"><?php echo dataBanco2data($ps['periodo_insc_i']); ?></strong>
                    </h6>
                    <h6>
                        ENCERRAMENTO DAS INSCRIÇÕES: <strong class="text-info"><?php echo dataBanco2data($ps['periodo_insc_f']); ?></strong>
                    </h6>
                    <h6>
                        DATA PARA RESULTADO: <strong class="text-info"><?php echo dataBanco2data($ps['data_resultado']); ?></strong>
                    </h6>
                    <h6>
                        SITE ONDE SAIRÁ O RESULTADO: <strong class="text-info"><?php echo $ps['local_resultado']; ?></strong>
                    </h6>
                    <h6>
                        LINK PARA O EDITAL: <a href="<?php echo $ps['edital']; ?>" class="btn btn-outline-info" target="_blank">ACESSAR</a>
                    </h6>
                    <h6>
                        INSCRIÇÃO ÚNICA (apenas pode escolher um cargo do edital para se inscrever): <strong class="text-info">
                            <?php
                            if ($ps['insc_unica']==1){
                                echo "<i class='text-success'>ATIVO</i>";
                            }else{
                                echo "<i class='text-danger'>DESATIVADO</i>";
                            }
                            ?>
                        </strong>
                    </h6>
                    <h6>
                        STATUS: <strong class="text-info">
                            <?php
                            if ($ps['status']==0){
                                echo "<i class='text-danger'>INATIVO</i>";
                            }
                            if ($ps['status']==1){
                                echo "<i class='text-success'>ATIVO</i>";
                            }
                            if ($ps['status']==2){
                                echo "<i class='text-warning'>ENCERRADO</i>";
                            }
                            ?>
                        </strong>
                    </h6>
                </blockquote>
                    <a class="btn btn-success btn-block" href="index.php?pg=Vpss_editar&id=<?php echo $ps['id']; ?>" title="Edite os dados dessa pessoa">
                        EDITAR
                    </a>
                </div>
            <!-- fim da col md 4 -->
            </div>
        </div>

<!--        --><?php
//        if(isset($_GET['id'])) {
//            $id_ps =$_GET['id'];
//            $sql = "SELECT * \n"
//                . "FROM mcu_ps_inscricao \n"
//                . "WHERE ((id_ps=?))\n"
//                . "ORDER BY cod_cadastro DESC";
//                global $pdo;
//                $consulta = $pdo->prepare($sql);
//                $consulta->bindParam(1, $id_ps);
//                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
//                $hist = $consulta->fetchAll();
//                $sql=null;
//                $consulta=null;
//
//        }
//        ?>
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados dos cargos
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="index.php?pg=Vcargo_editar&pss=<?php echo $_GET['id'];?>" class="btn btn-success">NOVO CARGO</a>
                            <hr>
                        </div>

                        <?php
                        $sql = "SELECT * FROM\n"
                            . "tbl_pss_cargo \n"
                            . "WHERE id_pss = ? ORDER BY \n"
                            . "id ASC";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->bindParam(1,$ps['id']);
                        $consulta->execute();
                        $lista = $consulta->fetchAll();
                        $sql=null;
                        $consulta=null;


                        foreach ($lista as $lt){ ?>

                        <div class="col-md-5" >
                            <div class="card">
                                <div class="card-header bg-info text-light">
                                    Cargos
                                </div>
                                <div class="card-body">
                                    <blockquote class="blockquote blockquote-info">
                                        <header>Código: <strong class="text-info"><?php echo $lt['id']; ?>&nbsp;&nbsp;</strong></header>
                                        <h6>
                                            Cargo/Função: <strong class="text-info"><?php echo $lt['nome_cargo']; ?></strong>
                                        </h6>
                                        <h6>
                                            Atribuição: <strong class="text-info"><?php echo $lt['atribuicao']; ?></strong>
                                        </h6>
                                        <h6>
                                            Data de cadastro: <strong class="text-info"><?php echo datahoraBanco2data($lt['data_ts']); ?></strong>
                                        </h6>
                                        <h6>
                                            Vagas: <strong class="text-info"><?php echo $lt['vagas']; ?></strong>
                                        </h6>
                                        <h6>
                                            Remuneração: <strong class="text-info"><?php echo $lt['remuneracao']; ?></strong>
                                        </h6>
                                        <h6>
                                            Jornada semanal: <strong class="text-info"><?php echo $lt['jornada_semanal']; ?></strong>
                                        </h6>
                                        <h6>
                                            Local de lotação: <strong class="text-info"><?php echo $lt['local_lotacao']; ?></strong>
                                        </h6>
                                        <h6>
                                            Pré requisito: <strong class="text-info"><?php echo $lt['pre_requisito']; ?></strong>
                                        </h6>
                                        <h6>
                                            Imagem para ilustrar: <img src="<?php echo $lt['imagem']; ?>" alt="" width="30%" class="img-fluid">
                                        </h6>
                                        <h6>
                                            Pedir anexos: <strong class="text-info">
                                                <?php
                                                if ($lt['p_anexo']==1){
                                                    echo "<i class='text-success'>COM ANEXO</i>";
                                                }else{
                                                    echo "<i class='text-danger'>SEM ANEXO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Anexo: <strong class="text-info"><?php echo $lt['anexo']; ?></strong>
                                        </h6>
                                        <h6>
                                            Tempo nessa função: <strong class="text-info">
                                                <?php
                                                if ($lt['p_tempo_funcao']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Tempo em outra função: <strong class="text-info">
                                                <?php
                                                if ($lt['p_tempo_outrafuncao']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Descrever habilitação: <strong class="text-info">
                                                <?php
                                                if ($lt['p_habilitacao']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Declarar se tem ensino elementar completo: <strong class="text-info">
                                                <?php
                                                if ($lt['p_elementar']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Declarar se tem ensino médio completo: <strong class="text-info">
                                                <?php
                                                if ($lt['p_medio']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Declarar deficiência: <strong class="text-info">
                                                <?php
                                                if ($lt['p_deficiencia']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Descrever curso: <strong class="text-info">
                                                <?php
                                                if ($lt['p_curso']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Declarar Período: <strong class="text-info">
                                                <?php
                                                if ($lt['p_periodo']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <h6>
                                            Declarar média global: <strong class="text-info">
                                                <?php
                                                if ($lt['p_media_global']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
<!--                                        //////////////////////////////////////////////-->

                                        <h6>
                                            Declarar pedagogia: <strong class="text-info">
                                                <?php
                                                if ($lt['p_pedagogia']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>

                                        <h6>
                                            Declarar pós na área: <strong class="text-info">
                                                <?php
                                                if ($lt['p_pos_graduacao_area']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>

                                        <h6>
                                            Declarar mestrado: <strong class="text-info">
                                                <?php
                                                if ($lt['p_mestrado']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>

                                        <h6>
                                            Declarar normal de nível médio (específico para educação infantil): <strong class="text-info">
                                                <?php
                                                if ($lt['p_normal_medio_eduinfantil']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>

                                        <h6>
                                            Declarar normal superior: <strong class="text-info">
                                                <?php
                                                if ($lt['p_normal_superior']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>

                                        <h6>
                                            Declarar magisterio: <strong class="text-info">
                                                <?php
                                                if ($lt['p_magisterio']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>

                                        <h6>
                                            Declarar magisterio a nível de 2º grau: <strong class="text-info">
                                                <?php
                                                if ($lt['p_magisterio2grau']==1){
                                                    echo "<i class='text-success'>SIM</i>";
                                                }else{
                                                    echo "<i class='text-danger'>NÃO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>

<!--                                        //////////////////////////////////////////////-->
                                        <h6>
                                            Status: <strong class="text-info">
                                                <?php
                                                if ($lt['status']==1){
                                                    echo "<i class='text-success'>ATIVO</i>";
                                                }else{
                                                    echo "<i class='text-danger'>INATIVO</i>";
                                                }
                                                ?>
                                            </strong>
                                        </h6>
                                        <a href="index.php?pg=Vcargo_editar&pss=<?php echo $_GET['id'];?>&id=<?php echo $lt['id']; ?>" class="btn btn-success btn-block">Editar</a>
                                    </blockquote>
                                </div>
                            </div>
                        </div>


                        <?php
                        $sql = "SELECT * FROM\n"
                            . "tbl_pss_inscricao \n"
                            . "WHERE id_cargo = ? ORDER BY \n"
                            . "data_ts ASC ";
                        global $pdo;
                        $consulta2 = $pdo->prepare($sql);
                        $consulta2->bindParam(1,$lt['id']);
                        $consulta2->execute();
                        $insc = $consulta2->fetchAll();
                        $sql=null;
                        $consulta2=null;
                        ?>

                        <div class="col-md-7" >
                            <script>
                                setTimeout(function(){
                                    $('#card<?php echo $lt['id'];?>').collapse();
                                }, 4000);
                            </script>
                            <div class="card" >
                                <div class="card-header bg-info text-light" data-toggle="collapse" data-target="#card<?php echo $lt['id'];?>" aria-expanded="false" aria-controls="card<?php echo $lt['id'];?>">
                                    Inscritos
                                </div>
                                <div class="card-body" >
                                    <table class="table table-sm table-striped table-responsive collapse show" id="card<?php echo $lt['id'];?>">
                                        <thead class='thead-dark'>
                                        <tr>
                                            <th>#</th>
                                            <th>NOME</th>
                                            <th>NASCIMENTO</th>
                                            <th>INSC.</th>
                                            <th>CIDADE</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <a href="index.php?pg=Vcargo_rel_<?php echo $lt['id'];?>_1" target="_blank" class="btn btn-success btn-block my-1">RELATÓRIO SINTÉTICO <?php echo $lt['nome_cargo']; ?></a>
                                        <a href="index.php?pg=Vcargo_rel_<?php echo $lt['id'];?>_1D" target="_blank" class="btn btn-success btn-block my-1">RELATÓRIO SINTÉTICO <?php echo $lt['nome_cargo']; ?> DEF.</a>
                                        <a href="index.php?pg=Vcargo_rel_<?php echo $lt['id'];?>_2" target="_blank"  class="btn btn-success btn-block my-1">RELATÓRIO ANALÍTICO <?php echo $lt['nome_cargo']; ?></a>
                                        <?php
                                        $ccc=0;
                                        foreach ($insc as $ins){
                                            if ($ins['status']==0){
                                                $status_cor="bg-danger";
                                            }else{
                                                $status_cor=" ";
                                            }
                                            $ccc++;
                                            ?>
                                            <tr class="<?php echo $status_cor;?>">
                                                <td><?php echo $ccc;?></td>
                                                <td><a href="index.php?pg=Vinsc&id=<?php echo $ins['id'];?>" target="_blank"><?php echo $ins['nome'];?></a></td>
                                                <td><?php echo dataBanco2data($ins['nascimento']);?></td>
                                                <td><?php echo datahoraBanco2data($ins['data_ts']);?></td>
                                                <td><?php echo $ins['cidade'];?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                            <div class="col-md-12" >
                                <hr>
                            </div>

                        <?php } ?>
                    </div>


                </div>
            </div>
        </div>


    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>