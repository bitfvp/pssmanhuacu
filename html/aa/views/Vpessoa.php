<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==2){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="Sistemas-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
$sorte=rand(1,10);
?>
<main class="container">
    <div class="card my-1">
        <div class="card-header bg-info text-light">
            <a href="index.php?pg=Vus" class="btn btn-sm btn-dark float-right">Voltar <<  </a>Dados
        </div>
        <div class="card-body">
            <?php
            if (isset($_GET['id']) and is_numeric($_GET['id'])){
                $pessoa=fncgetusuario($_GET['id']);
            }else{
                echo "erro";
            }
            ?>
            <h5>Nome:
                <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>CPF:
                <strong class="text-info"><?php echo mask($pessoa['cpf'], '###.###.###-##'); ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>Nascimento:
                <strong class="text-info"><?php echo dataBanco2data($pessoa['nascimento']); ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>E-mail:
                <strong class="text-info"><?php echo $pessoa['email']; ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>Endereço:
                <strong class="text-info"><?php echo $pessoa['endereco']; ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>Telefone:
                <strong class="text-info"><?php echo $pessoa['telefone']; ?>&nbsp;&nbsp;</strong>
            </h5>
            <hr>
        </div>
    </div>

    <div class="card my-1">
        <div class="card-header bg-success text-light">
            Inscrições
        </div>
        <div class="card-body">
                <?php
                $sql = "SELECT * \n"
                    . "FROM tbl_pss_inscricao \n"
                    . "WHERE ( tbl_pss_inscricao.pessoa=? )\n"
                    . "ORDER BY tbl_pss_inscricao.data_ts DESC";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $_GET['id']);
                $consulta->execute();
                $inscs = $consulta->fetchAll();
                $sql = null;
                $consulta = null;

                foreach ($inscs as $insc){
                    $cr=fncgetcargo($insc['id_cargo']);
                    $pss=fncgetpss($cr['id_pss']);
                    if ($insc['status']==0){
                        echo "<h1 class='float-right badge badge-danger'>DESISTIU DO CARGO</h1>";
                        $status_cor= "danger";
                    }else{
                        $status_cor= "info";
                    }
                    echo "<div class='blockquote blockquote-{$status_cor}'>";

                    echo "<h5>";
                    echo $cr['nome_cargo'];
                    echo "</h5>";

                    echo "<h6>Data da inscrição: ";
                    echo datahoraBanco2data($insc['data_ts']);
                    echo "</h6>";

                    echo "<h6>Nome: ";
                    echo $insc['nome'];
                    echo "</h6>";

                    echo "<h6>Naturalidade: ";
                    echo $insc['naturalidade'];
                    echo "</h6>";

                    echo "<h6>Sexo: ";
                    if ($insc['sexo']==1){
                        echo "Feminino";
                    }
                    if ($insc['sexo']==2){
                        echo "Masculino";
                    }
                    if ($insc['sexo']==3){
                        echo "Não declarado";
                    }
                    echo "</h6>";

                    echo "<h6>CPF: ";
                    echo $insc['cpf'];
                    echo "</h6>";

                    echo "<h6>RG: ";
                    echo $insc['rg'];
                    echo "</h6>";

                    echo "<h6>Nascimento: ";
                    echo dataBanco2data($insc['nascimento']);
                    echo "</h6>";

                    echo "<h6>Endereço: ";
                    echo $insc['endereco'];
                    echo "</h6>";

                    echo "<h6>Bairro: ";
                    echo $insc['bairro'];
                    echo "</h6>";

                    echo "<h6>Cidade: ";
                    echo $insc['cidade'];
                    echo "</h6>";

                    echo "<h6>CEP: ";
                    echo $insc['cep'];
                    echo "</h6>";

                    echo "<h6>Telefone: ";
                    echo $insc['telefone'];
                    echo "</h6>";

                    echo "<h6>E-mail: ";
                    echo $insc['email'];
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['deficiencia']==1){
                        echo "Possui deficiência";
                    }else{
                        echo "Não possui deficiência";
                    }
                    echo "</h6>";

                    echo "<h6>";
                    echo $insc['deficiencia_desc'];
                    echo "</h6>";

                    echo "<h6>Tempo em dias nessa função: ";
                    echo $insc['p_tempo_funcao'];
                    echo "</h6>";

                    echo "<h6>tempo em dias em outra função no município: ";
                    echo $insc['p_tempo_outrafuncao'];
                    echo "</h6>";

                    echo "<h6>Habilitação/formação: ";
                    echo $insc['p_habilitacao'];
                    echo "</h6>";

                    echo "<h6>Curso: ";
                    echo $insc['p_curso'];
                    echo "</h6>";

                    echo "<h6>Período: ";
                    echo $insc['p_periodo'];
                    echo "</h6>";

                    echo "<h6>Média global: ";
                    echo $insc['p_media_global'];
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['p_elementar']==1){
                        echo "Possui fundamental";
                    }else{
                        echo "";
                    }
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['p_medio']==1){
                        echo "Possui ensino médio";
                    }else{
                        echo "";
                    }
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['p_pedagogia']==1){
                        echo "Possui pedagogia";
                    }else{
                        echo "";
                    }
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['p_pos_graduacao_area']==1){
                        echo "Possui pos graduacao na area";
                    }else{
                        echo "";
                    }
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['p_mestrado']==1){
                        echo "Possui mestrado";
                    }else{
                        echo "";
                    }
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['p_normal_superior']==1){
                        echo "Possui normal superior";
                    }else{
                        echo "";
                    }
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['p_magisterio']==1){
                        echo "Possui magistério";
                    }else{
                        echo "";
                    }
                    echo "</h6>";

                    echo "<blockquote>";

                            echo "<table class='table table-sm table-striped table-responsive mt-3'>";
                            echo "<thead class='thead-dark'>";
                            echo "<tr>";
                            echo "<th>DOCUMENTO</th>";
                            echo "<th>ARQUIVO</th>";
                            echo "<th>DATA</th>";
                            echo "<th>AÇÃO</th>";
                            echo "</tr>";
                            echo "</thead>";
                            echo "<tbody>";

                    $sql = "SELECT * FROM `tbl_pss_inscricao_dados` where id_inscricao='{$insc['id']}' and status='1' ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute();
                    $dados = $consulta->fetchAll();//$total[0]
                    $sql = null;
                    $consulta = null;

                    foreach ($dados as $dado){
                        echo "<tr>";
                        echo "<td>".fncgetdocumento($dado['documento'])['documento']."</td>";
                        echo "<td>".$dado['arquivo'].".".$dado['extensao']."</td>";
                        echo "<td>".datahoraBanco2data($dado['data_cadastro'])."</td>";

                        echo "<td>";
                        $link="../dados/pss/".$pss['id']."/" . $cr['id'] . "/" . $dado['id_inscricao'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                        echo "<a href='" . $link . "' target='_blank' class='btn btn-info btn-sm'>Visualizar</a>";
                        echo "</td>";

                        echo "</tr>";
                    }


                            echo "</tbody>";
                            echo "</table>";

                    echo "</blockquote>";


                    echo "</div><hr>";
                }
                ?>

        </div>
    </div>



</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>