<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==2){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
// Recebe

$id_cargo= "38";
$cr=fncgetcargo($id_cargo);
$ps=fncgetpss($cr['id_pss']);
$sql = "SELECT * \n"
    . "FROM tbl_pss_inscricao \n"
    . "WHERE id_cargo=? and status=1 and deficiencia=1 order by tbl_pss_inscricao.p_medio DESC, tbl_pss_inscricao.tem_anexo DESC, tbl_pss_inscricao.nascimento ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id_cargo);
    $consulta->execute();
    $inscs = $consulta->fetchAll();
    $inscscount = $consulta->rowCount();
    $sql=null;
    $consulta=null;
?>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h2 class="text-left ml-4 my-0"><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h2>
            <h6 class="text-left ml-4 my-0">Lei Provincial n°. 2407 de 5/XI/1877 - Área: 628.43 km² - Altitude: 612 metros</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</h6>
            <h4 class="my-0"><strong><?php echo $ps['secretaria'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo $ps['nome_ps'];?> <?php echo $ps['cod_ps'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo $cr['nome_cargo'];?></strong></h4>
            <h5 class="my-0"><strong class="text-uppercase"><?php echo $cr['local_lotacao'];?></strong></h5>
            <h6 class="my-0"><strong>DEFICIENTE </strong></h6>
            <script type="text/javascript">
                $(document).ready(function() {
                    document.title = '<?php echo $cr['nome_cargo']; ?> DEFICIENTE';
                });
            </script>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">
<!--            <i>-->
<!--                Legendas:-->
<!--            </i><br>-->
<!--                <i class="text-info">-->
<!--                    -DDF: DIAS DECLARADOS NA FUNÇÃO<BR>-->
<!--                    -CNMINF: CURSO NORMAL DE NÍVEL MÉDIO (ESPECÍFICO PARA EDUCAÇÃO INFANTIL)<BR>-->
<!--                </i>-->

            <br>
<table class="table table-sm table-striped table-bordered">
    <thead class='thead-dark' style="display: table-row-group;">
    <tr>
        <th>NOME</th>
<!--        <th>DATA DA INSCRIÇÃO</th>-->
        <th>CPF</th>
        <th>CIDADE</th>
        <th>MÉDIO COMPLETO</th>
        <th>POSSUI ANEXO</th>
        <th>DATA NASC.</th>
        <th>Nº</th>
    </tr>
    </thead>
    <tbody>

    <?php
    $ccc=0;
    foreach ($inscs as $insc){

//        $ppontos=0;
//        if ($insc['p_magisterio2grau']==1){
//            $diaspontos=5;
//                $ppontos=$ppontos+$diaspontos;
//        }else{
//            $ppontos=$ppontos+0;
//        }
//        if ($insc['p_normal_medio_eduinfantil']==1){
//            $diaspontos=5;
//            $ppontos=$ppontos+$diaspontos;
//        }else{
//            $ppontos=$ppontos+0;
//        }
//        if ($insc['p_tempo_funcao']>1){
//            $diaspontos=$insc['p_tempo_funcao']/365;
//            if ($diaspontos>=5){
//                $ppontos=$ppontos+5;
//            }else{
//                $diaspontos=floor(number_format($diaspontos, 2));
//                $ppontos=$ppontos+$diaspontos;
//            }
//        }else{
//            $ppontos=$ppontos+0;
//        }
//
//        try{
//            $sql="UPDATE tbl_pss_inscricao SET pontos=:pontos "
//                ."WHERE id=:id ";
//
//            global $pdo;
//            $insere=$pdo->prepare($sql);
//            $insere->bindValue(":pontos", $ppontos);
//            $insere->bindValue(":id", $insc['id']);
//            $insere->execute();
//        }catch ( PDOException $error_msg){
//            echo 'Erro'. $error_msg->getMessage();
//        }



        $ep=0;
        $ccc++;
        echo "<tr>";

        echo "<td>";
        echo $insc['nome'];
        echo "</td>";

//        echo "<td>";
//        echo datahoraBanco2data($insc['data_ts']);
//        echo "</td>";

        echo "<th>";
        $cpf=$insc['cpf'];
        echo $cpf[0].$cpf[1].$cpf[2]."......-".$cpf[9].$cpf[10];
//        echo $insc['cpf'];
        echo "</th>";

        echo "<td>";
        echo $insc['cidade'];
        echo "</td>";

        echo "<td style='white-space: nowrap;'>";
        if ($insc['p_medio']==1){
            echo "SIM";
        }else{
            echo "NÃO";
        }
        echo "</td>";

        echo "<td>";
        if ($insc['tem_anexo']==1){
            echo "SIM";
        }else{
            echo "NÃO";
        }
        echo "</td>";


        echo "<td>";
        if (Calculo_Idade($insc['nascimento'])>=18 and Calculo_Idade($insc['nascimento'])<=70){
            echo dataBanco2data($insc['nascimento'])." <br> ".Calculo_Idade($insc['nascimento'])." anos";
        }else{
            echo "Erro de preenchimento";
            $ep=1;
            $ccc=$ccc-1;
        }
        echo "</td>";

        echo "<td>";
        if ($ep==1){
            echo "**";
        }else{
            echo $ccc;
        }

//        echo "---";
//        echo $insc['pontos'];
//        echo "---";

        echo "</td>";


        echo "</tr>";
    }
    ?>

    </tbody>
</table>


        </div>
    </div>
    <br>
</main>

</body>
</html>