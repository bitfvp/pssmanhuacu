<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==2){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="Sistemas-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">


    <?php
    if (isset($_GET['id']) and is_numeric($_GET['id'])){
        $insc=fncgetinscricao($_GET['id']);
    }else{
        echo "erro";
        exit();
    }

    $cr=fncgetcargo($insc['id_cargo']);
    $pss=fncgetpss($cr['id_pss']);

    if ($insc['status']==0){
        $status_cor="danger";
    }else{
        $status_cor="info";
    }
    ?>

    <div class="card my-1">
        <div class="card-header bg-<?php echo $status_cor;?> text-light">

            <?php
            echo $cr['nome_cargo'];
            if ($insc['status']==0){
                echo "<h1 class='float-right badge badge-danger'>DESISTIU DO CARGO</h1>";
            }
            ?>
        </div>
        <div class="card-body">
            <div class='row'>
            <?php
                echo "<div class='col-md-5'>";

                    echo "<h6>Data da inscrição: ";
                    echo datahoraBanco2data($insc['data_ts']);
                    echo "</h6>";

                    echo "<strong class='text-info'>Nome: ";
                    echo $insc['nome'];
                    echo "</strong>";

                    echo "<h6>Naturalidade: ";
                    echo $insc['naturalidade'];
                    echo "</h6>";

                    echo "<h6>Sexo: ";
                    if ($insc['sexo']==1){
                        echo "Feminino";
                    }
                    if ($insc['sexo']==2){
                        echo "Masculino";
                    }
                    if ($insc['sexo']==3){
                        echo "Não declarado";
                    }
                    echo "</h6>";

                    echo "<h6>CPF: ";
                    echo $insc['cpf'];
                    echo "</h6>";

                    echo "<h6>RG: ";
                    echo $insc['rg'];
                    echo "</h6>";

                    echo "<h6>Nascimento: ";
                    echo dataBanco2data($insc['nascimento']);
                    echo "</h6>";

                    echo "<h6>Endereço: ";
                    echo $insc['endereco'];
                    echo "</h6>";

                    echo "<h6>Bairro: ";
                    echo $insc['bairro'];
                    echo "</h6>";

                    echo "<h6>Cidade: ";
                    echo $insc['cidade'];
                    echo "</h6>";

                    echo "<h6>CEP: ";
                    echo $insc['cep'];
                    echo "</h6>";

                    echo "<h6>Telefone: ";
                    echo $insc['telefone'];
                    echo "</h6>";

                    echo "<h6>E-mail: ";
                    echo $insc['email'];
                    echo "</h6>";

                    echo "<h6>";
                    if ($insc['deficiencia']==1){
                        echo "Possui deficiência";
                    }else{
                        echo "Não possui deficiência";
                    }
                    echo "</h6>";

                    echo "<h6>";
                    echo $insc['deficiencia_desc'];
                    echo "</h6>";

                    echo "<h6>Tempo em dias nessa função: ";
                    echo $insc['p_tempo_funcao'];
                    echo "</h6>";

                    echo "<h6>tempo em dias em outra função: ";
                    echo $insc['p_tempo_outrafuncao'];
                    echo "</h6>";

                    echo "<h6>Habilitação/formação: ";
                    echo $insc['p_habilitacao'];
                    echo "</h6>";

                    echo "<h6>Curso: ";
                    echo $insc['p_curso'];
                    echo "</h6>";

                    echo "<h6>Período: ";
                    echo $insc['p_periodo'];
                    echo "</h6>";

                    echo "<h6>Média global: ";
                    echo $insc['p_media_global'];
                    echo "</h6>";

            echo "<h6>";
            if ($insc['p_elementar']==1){
                echo "Possui fundamental";
            }else{
                echo "";
            }
            echo "</h6>";

            echo "<h6>";
            if ($insc['p_medio']==1){
                echo "Possui ensino médio";
            }else{
                echo "";
            }
            echo "</h6>";

            echo "<h6>";
            if ($insc['p_pedagogia']==1){
                echo "Possui pedagogia";
            }else{
                echo "";
            }
            echo "</h6>";

            echo "<h6>";
            if ($insc['p_pos_graduacao_area']==1){
                echo "Possui pos graduacao na area";
            }else{
                echo "";
            }
            echo "</h6>";

            echo "<h6>";
            if ($insc['p_mestrado']==1){
                echo "Possui mestrado";
            }else{
                echo "";
            }
            echo "</h6>";

            echo "<h6>";
            if ($insc['p_normal_superior']==1){
                echo "Possui normal superior";
            }else{
                echo "";
            }
            echo "</h6>";

            echo "<h6>";
            if ($insc['p_magisterio']==1){
                echo "Possui magistério";
            }else{
                echo "";
            }
            echo "</h6>";

                echo "</div>";

                echo "<div class='col-md-7'>";
                            echo "<table class='table table-sm table-striped table-responsive'>";
                            echo "<thead class='thead-dark'>";
                            echo "<tr>";
                            echo "<th>DOCUMENTO</th>";
                            echo "<th>ARQUIVO</th>";
                            echo "<th>DATA</th>";
                            echo "<th>AÇÃO</th>";
                            echo "</tr>";
                            echo "</thead>";
                            echo "<tbody>";

                    $sql = "SELECT * FROM `tbl_pss_inscricao_dados` where id_inscricao='{$insc['id']}' and status='1' ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute();
                    $dados = $consulta->fetchAll();//$total[0]
                    $sql = null;
                    $consulta = null;

                    foreach ($dados as $dado){
                        echo "<tr>";
                        echo "<td>".fncgetdocumento($dado['documento'])['documento']."</td>";
                        echo "<td>".$dado['arquivo'].".".$dado['extensao']."</td>";
                        echo "<td>".datahoraBanco2data($dado['data_cadastro'])."</td>";

                        echo "<td>";
                        $link="../dados/pss/".$pss['id']."/" . $cr['id'] . "/" . $dado['id_inscricao'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                        echo "<a href='" . $link . "' target='_blank' class='btn btn-info btn-sm'>Visualizar</a>";
                        echo "</td>";

                        echo "</tr>";
                    }


                            echo "</tbody>";
                            echo "</table>";

                    echo "</div>";
                ?>
            </div><!--    fim de row        -->
        </div>
    </div>

    <div class="card my-2">
        <div class="card-header bg-success text-dark">
            Reservado para controle
        </div>
        <div class="card-body">
            <form class="frmgrid" action="index.php?pg=Vinsc&id=<?php echo $insc['id'];?>&aca=inscr_verifica" method="post">
                <div class="row">
                    <div class="col-md-2">
                        <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $insc['id']; ?>"/>
                        <label for="v_cpf">Anexou CPF:</label>
                        <select name="v_cpf" id="v_cpf" class="form-control">// vamos criar a visualização de sexo
                            <option selected="" value="<?php if ($insc['v_cpf'] == "") {
                                $z = 0;
                                echo $z;
                            } else {
                                echo $insc['v_cpf'];
                            } ?>">
                                <?php
                                if ($insc['v_cpf'] == 0) {
                                    echo "Não";
                                }
                                if ($insc['v_cpf'] == 1) {
                                    echo "Sim";
                                }
                                ?>
                            </option>
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="v_rg">Anexou RG:</label>
                        <select name="v_rg" id="v_rg" class="form-control">// vamos criar a visualização de sexo
                            <option selected="" value="<?php if ($insc['v_rg'] == "") {
                                $z = 0;
                                echo $z;
                            } else {
                                echo $insc['v_rg'];
                            } ?>">
                                <?php
                                if ($insc['v_rg'] == 0) {
                                    echo "Não";
                                }
                                if ($insc['v_rg'] == 1) {
                                    echo "Sim";
                                }
                                ?>
                            </option>
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="v_certificado">Anexou Certificado:</label>
                        <select name="v_certificado" id="v_certificado" class="form-control">// vamos criar a visualização de sexo
                            <option selected="" value="<?php if ($insc['v_certificado'] == "") {
                                $z = 0;
                                echo $z;
                            } else {
                                echo $insc['v_certificado'];
                            } ?>">
                                <?php
                                if ($insc['v_certificado'] == 0) {
                                    echo "Não";
                                }
                                if ($insc['v_certificado'] == 1) {
                                    echo "Sim";
                                }
                                ?>
                            </option>
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="v_curriculo">Anexou Currículo:</label>
                        <select name="v_curriculo" id="v_curriculo" class="form-control">// vamos criar a visualização de sexo
                            <option selected="" value="<?php if ($insc['v_curriculo'] == "") {
                                $z = 0;
                                echo $z;
                            } else {
                                echo $insc['v_curriculo'];
                            } ?>">
                                <?php
                                if ($insc['v_curriculo'] == 0) {
                                    echo "Não";
                                }
                                if ($insc['v_curriculo'] == 1) {
                                    echo "Sim";
                                }
                                ?>
                            </option>
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="v_esperiencia">Anexou Experiência:</label>
                        <select name="v_esperiencia" id="v_esperiencia" class="form-control">// vamos criar a visualização de sexo
                            <option selected="" value="<?php if ($insc['v_esperiencia'] == "") {
                                $z = 0;
                                echo $z;
                            } else {
                                echo $insc['v_esperiencia'];
                            } ?>">
                                <?php
                                if ($insc['v_esperiencia'] == 0) {
                                    echo "Não";
                                }
                                if ($insc['v_esperiencia'] == 1) {
                                    echo "Sim";
                                }
                                ?>
                            </option>
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>

                    <div class="col-md-2">
                        <label for="v_tfm">Pontos por TFM:</label>
                        <input id="v_tfm" type="number" class="form-control" name="v_tfm" value="<?php echo $insc['v_tfm']; ?>" step="0.1" min="0" required />
                    </div>

                    <div class="col-md-2">
                        <label for="v_tofm">Pontos por TOFM:</label>
                        <input id="v_tofm" type="number" class="form-control" name="v_tofm" value="<?php echo $insc['v_tofm']; ?>" step="0.1" min="0" required />
                    </div>

                    <div class="col-md-2">
                        <label for="v_cursos">Pontos por cursos:</label>
                        <input autocomplete="off" id="v_cursos" type="text" class="form-control" name="v_cursos" value="<?php echo $insc['v_cursos']; ?>" required />
                        <script>
                            $(document).ready(function(){
                                $('#v_cursos').mask('000000', {reverse: false});
                            });
                        </script>
                    </div>

                    <div class="col-md-2">
                        <label for="v_especializacao">Pontos por espec.:</label>
                        <input autocomplete="off" id="v_especializacao" type="text" class="form-control" name="v_especializacao" value="<?php echo $insc['v_especializacao']; ?>" required />
                        <script>
                            $(document).ready(function(){
                                $('#v_especializacao').mask('000000', {reverse: false});
                            });
                        </script>
                    </div>

                    <div class="col-md-2">
                        <label for="v_apto">Apto:</label>
                        <select name="v_apto" id="v_apto" class="form-control">// vamos criar a visualização de sexo
                            <option selected="" value="<?php if ($insc['v_apto'] == "") {
                                $z = 0;
                                echo $z;
                            } else {
                                echo $insc['v_apto'];
                            } ?>">
                                <?php
                                if ($insc['v_apto'] == 0) {
                                    echo "Não";
                                }
                                if ($insc['v_apto'] == 1) {
                                    echo "Sim";
                                }
                                ?>
                            </option>
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>

                    <div class="col-md-12 my-2">
                        <?php if ($insc['verificado'] == 1) {
                            $vvv=datahoraBanco2data($insc['v_data']);
                            echo "<i class='text-success'>Verificado em {$vvv}</i>";
                        }else{
                            echo "<i class='text-danger'>Ainda não verificado</i>";
                        }
                        ?>
                    </div>

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-sm btn-outline-dark btn-block">SALVAR</button>
                    </div>

            </form>
        </div>
    </div>


    <div class="card my-3">
        <div class="card-header bg-info text-light">
            Sobre conta que fez a inscrição
        </div>
        <div class="card-body">
            <?php
                $pessoa=fncgetusuario($insc['pessoa']);
            ?>
            <h5>Nome:
                <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>CPF:
                <strong class="text-info"><?php echo mask($pessoa['cpf'], '###.###.###-##'); ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>Nascimento:
                <strong class="text-info"><?php echo dataBanco2data($pessoa['nascimento']); ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>E-mail:
                <strong class="text-info"><?php echo $pessoa['email']; ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>Endereço:
                <strong class="text-info"><?php echo $pessoa['endereco']; ?>&nbsp;&nbsp;</strong>
            </h5>
            <h5>Telefone:
                <strong class="text-info"><?php echo $pessoa['telefone']; ?>&nbsp;&nbsp;</strong>
            </h5>
            <hr>
        </div>
    </div>



</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>