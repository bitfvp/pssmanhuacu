<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==2){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="Sistemas-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
$sorte=rand(1,10);
?>
<main class="container">
    <div class="card">
        <div class="card-header bg-info text-light">
            Lista de pessoas cadastradas
        </div>
        <div class="card-body">


            <table class="table table-striped table-sm table-responsive">
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>inscrito em</th>
                    <th>Nome</th>
                    <th>Nascimento</th>
                    <th>CPF</th>
                    <th>Perfil</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sql = "SELECT * FROM\n"
                    . "tbl_users \n"
                    . "ORDER BY\n"
                    . "tbl_users.nome ASC";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute();
                $lista = $consulta->fetchAll();
                $sql=null;
                $consulta=null;

                $cont=0;
                foreach ($lista as $lt) {
                    $cont++;
                    $idd=$lt['id'];
                    $nascimento = dataBanco2data($lt['nascimento']);
                    $data_ts = datahoraBanco2data($lt['data_cadastro']);
                    $cpf = mask($lt['cpf'], '###.###.###-##');

                    echo "<tr>";
                    echo "<td>{$cont}</td>";
                    echo "<td>{$data_ts}</td>";
                    echo "<td>{$idd}-<a href='index.php?pg=Vpessoa&id={$lt['id']}'>{$lt['nome']}</a></td>";
                    echo "<td>{$nascimento}</td>";
                    echo "<td>{$cpf}</td>";

                    if ($lt['nivel_acesso']==1){echo "<td class='text-muted bg-info'>Comum</td>";}
                    if ($lt['nivel_acesso']==2){echo "<td class='text-muted bg-success'>Administrador</td>";}

                    echo "<td><a href='index.php?pg=Vinscritos&aca=resetsenha&idd={$idd}' class='btn btn-sm btn-outline-warning'>Redefinir Senha</a></td>";

                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>


</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>