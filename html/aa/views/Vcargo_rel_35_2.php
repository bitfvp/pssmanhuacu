<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==2){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
// Recebe

$id_cargo= "35";
$cr=fncgetcargo($id_cargo);
$ps=fncgetpss($cr['id_pss']);
$sql = "SELECT * \n"
    . "FROM tbl_pss_inscricao \n"
    . "WHERE id_cargo=? and status=1 order by tbl_pss_inscricao.pontos DESC, tbl_pss_inscricao.nascimento ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id_cargo);
    $consulta->execute();
    $inscs = $consulta->fetchAll();
    $inscscount = $consulta->rowCount();
    $sql=null;
    $consulta=null;
?>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h2 class="text-left ml-4 my-0"><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h2>
            <h6 class="text-left ml-4 my-0">Lei Provincial n°. 2407 de 5/XI/1877 - Área: 628.43 km² - Altitude: 612 metros</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</h6>
            <h4 class="my-0"><strong><?php echo $ps['secretaria'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo $ps['nome_ps'];?> <?php echo $ps['cod_ps'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo $cr['nome_cargo'];?></strong></h4>
            <h5 class="my-0"><strong class="text-uppercase"><?php echo $cr['local_lotacao'];?></strong></h5>
<!--            <h6 class="my-0"><strong>RELATÓRIO ORDENADOS POR DDFM, DDOFM E IDADE </strong></h6>-->
            <h6 class="my-0"><strong>GERADO EM: <?php echo datahoraBanco2data(dataNow());?> </strong></h6>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">

    <?php
    $ccc=0;
    foreach ($inscs as $insc){
        $ccc++;
        echo "<hr style='border: #0f6674 dashed;'>";
        echo "<div>";
        echo "<div>";

        echo "<strong class='float-left'>";
        echo "POSIÇÃO QUANDO FOI GERADO ESSE RELATÓRIO: ";
        echo "<i>";
        echo $ccc;
        echo "</i>";
        echo "</strong>";
        echo "<br>";
///

        echo "<strong class='float-left'>";
        echo "NOME:";
        echo "<i>";
//        echo "<td><a href='index.php?pg=Vinsc&id={$insc['id']}' target='_blank'>";
//        echo $insc['nome'];
//        echo "</a>";
        echo "<td>";
        echo $insc['nome'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "DATA DE INSCRIÇÃO NA VAGA: ";
        echo "<i>";
        echo datahoraBanco2data($insc['data_ts']);
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "NATURALIDADE: ";
        echo "<i>";
        echo $insc['naturalidade'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "SEXO: ";
        echo "<i>";
        if ($insc['sexo']==1){
            echo "Feminino";
        }
        if ($insc['sexo']==2){
            echo "Masculino";
        }
        if ($insc['sexo']==3){
            echo "Não declarado";
        }
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "CPF: ";
        echo "<i>";
        echo $insc['cpf'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "RG: ";
        echo "<i>";
        echo $insc['rg'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "NASCIMENTO: ";
        echo "<i>";
        echo dataBanco2data($insc['nascimento'])." ".Calculo_Idade($insc['nascimento'])." anos";
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "ENDEREÇO: ";
        echo "<i>";
        echo $insc['endereco'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "BAIRRO: ";
        echo "<i>";
        echo $insc['bairro'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "CIDADE: ";
        echo "<i>";
        echo $insc['cidade'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "CEP: ";
        echo "<i>";
        echo $insc['cep'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "TELEFONE: ";
        echo "<i>";
        echo $insc['telefone'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "E-MAIL: ";
        echo "<i>";
        echo $insc['email'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";


        echo "<strong class='float-left'>";
        echo "TEMPO DECLARADO EM DIAS NESSA FUNÇÃO NO MUNICÍPIO: ";
        echo "<i>";
        if ($insc['p_tempo_funcao']>1){
            echo $insc['p_tempo_funcao']." dias";
        }else{
            if ($insc['p_tempo_funcao']==1){
                echo $insc['p_tempo_funcao']." dia";
            }else{
                echo "Zero";
            }
        }
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "TEMPO DECLARADO EM DIAS EM OUTRA FUNÇÃO NO MUNICÍPIO: ";
        echo "<i>";
        if ($insc['p_tempo_outrafuncao']>1){
            echo $insc['p_tempo_outrafuncao']." dias";
        }else{
            if ($insc['p_tempo_outrafuncao']==1){
                echo $insc['p_tempo_outrafuncao']." dia";
            }else{
                echo "Zero";
            }
        }
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "ENSINO ELEMENTAR: ";
        echo "<i>";
        if ($insc['p_elementar']==1){
            echo "Possui";
        }else{
            echo "Não possui";
        }
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "ENSINO MÉDIO: ";
        echo "<i>";
        if ($insc['p_medio']==1){
            echo "Possui";
        }else{
            echo "Não possui";
        }
        echo "</i>";
        echo "</strong>";
        echo "<br>";


        echo "<br>";

//        echo "<strong class='float-left'>";
//        echo "HABILITAÇÃO, LICENCIATURA OU GRADUAÇÃO: ";
//        echo "<i>";
//        echo $insc['p_habilitacao'];
//        echo "</i>";
//        echo "</strong>";
//        echo "<br>";
//        echo "<br>";
//
//        echo "<strong class='float-left'>";
//        echo "CURSO: ";
//        echo "<i>";
//        echo $insc['p_curso'];
//        echo "</i>";
//        echo "</strong>";
//        echo "<br>";
//        echo "<br>";
//
//        echo "<strong class='float-left'>";
//        echo "PERÍODO: ";
//        echo "<i>";
//        echo $insc['p_periodo'];
//        echo "</i>";
//        echo "</strong>";
//        echo "<br>";
//        echo "<br>";
//
//        echo "<strong class='float-left'>";
//        echo "MEDIA GLOBAL: ";
//        echo "<i>";
//        echo $insc['p_media_global'];
//        echo "</i>";
//        echo "</strong>";
//        echo "<br>";
//        echo "<br>";

        $pessoa=fncgetusuario($insc['pessoa']);

        echo "<strong class='float-left'>";
        echo "DETALHES DA CONTA QUE FEZ A INSCRIÇÃO";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "NOME DO USUÁRIO: ";
        echo "<i>";
        echo $pessoa['nome'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "CPF: ";
        echo "<i>";
        echo $pessoa['cpf'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "NASCIMENTO: ";
        echo "<i>";
        echo dataBanco2data($pessoa['nascimento']). " ". Calculo_Idade($pessoa['nascimento'])." anos";
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "E-MAIL: ";
        echo "<i>";
        echo $pessoa['email'];
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "<strong class='float-left'>";
        echo "QUANDO SE CADASTROU: ";
        echo "<i>";
        echo datahoraBanco2data($pessoa['data_cadastro']);
        echo "</i>";
        echo "</strong>";
        echo "<br>";

        echo "</div>";
        echo "</div>";
    }
    ?>



        </div>
    </div>
    <br>
</main>

</body>
</html>