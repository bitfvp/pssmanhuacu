<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==2){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
// Recebe

$id_cargo= "34";
$cr=fncgetcargo($id_cargo);
$ps=fncgetpss($cr['id_pss']);
$sql = "SELECT * \n"
    . "FROM tbl_pss_inscricao \n"
    . "WHERE id_cargo=? and status=1 and deficiencia=1 order by tbl_pss_inscricao.p_elementar DESC, tbl_pss_inscricao.pontos DESC, tbl_pss_inscricao.nascimento ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id_cargo);
    $consulta->execute();
    $inscs = $consulta->fetchAll();
    $inscscount = $consulta->rowCount();
    $sql=null;
    $consulta=null;
?>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h2 class="text-left ml-4 my-0"><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h2>
            <h6 class="text-left ml-4 my-0">Lei Provincial n°. 2407 de 5/XI/1877 - Área: 628.43 km² - Altitude: 612 metros</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</h6>
            <h4 class="my-0"><strong><?php echo $ps['secretaria'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo $ps['nome_ps'];?> <?php echo $ps['cod_ps'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo $cr['nome_cargo'];?></strong></h4>
            <h5 class="my-0"><strong class="text-uppercase"><?php echo $cr['local_lotacao'];?></strong></h5>
            <h6 class="my-0"><strong>DEFICIENTE </strong></h6>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">
            <i>
                Legendas:
            </i><br>
                <i class="text-info">
                    -DDF: Dias declarados na função<br>
                </i>

            <br>
<table class="table table-sm table-striped table-bordered">
    <thead class='thead-dark'>
    <tr>
        <th>NOME</th>
        <th>DATA DA INSCRIÇÃO</th>
        <th>CPF</th>
        <th>CIDADE</th>
        <th>ELEMENTAR</th>
        <th>DDF</th>
        <th>NASC.</th>
        <th>#</th>
    </tr>
    </thead>
    <tbody>

    <?php
    $ccc=0;
    foreach ($inscs as $insc){

        $ep=0;
        $ccc++;
        echo "<tr>";

        echo "<td>";
        echo $insc['nome'];
        echo "</td>";

        echo "<td>";
        echo datahoraBanco2data($insc['data_ts']);
        echo "</td>";

        echo "<td>";
        $cpf=$insc['cpf'];
        echo $cpf[0].$cpf[1].$cpf[2]."......-".$cpf[9].$cpf[10];
//        echo $insc['cpf'];
        echo "</td>";

        echo "<td>";
        echo $insc['cidade'];
        echo "</td>";


        echo "<td>";
        if ($insc['p_elementar']==1){
            echo "SIM";
        }else{
            echo "NÃO";
        }

        echo "</td>";

        echo "<td>";
        if ($insc['p_tempo_funcao']>1){
            echo $insc['p_tempo_funcao']." dias <br>";
             $pontos=$insc['p_tempo_funcao']/365;
             if ($pontos>=5){
                 echo "5 pontos";
             }else{
                 echo floor(number_format($pontos, 2))." pontos";
             }
        }else{
            if ($insc['p_tempo_funcao']==1){
                echo $insc['p_tempo_funcao']." dia";
            }
            if ($insc['p_tempo_funcao']==0){
                echo "0";
            }
        }
        echo "</td>";


        echo "<td>";
        if (Calculo_Idade($insc['nascimento'])>=18 and Calculo_Idade($insc['nascimento'])<=70){
            echo dataBanco2data($insc['nascimento'])." <br> ".Calculo_Idade($insc['nascimento'])." anos";
        }else{
            echo "Erro de preenchimento";
            $ep=1;
            $ccc=$ccc-1;
        }
        echo "</td>";

        echo "<td>";
        if ($ep==1){
            echo "**";
        }else{
            echo $ccc;
        }

//        echo "---";
//        echo $insc['pontos'];
//        echo "---";

        echo "</td>";


        echo "</tr>";
    }
    ?>

    </tbody>
</table>


        </div>
    </div>
    <br>
</main>

</body>
</html>