<?php
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==2){
    //acesso liberado
}else{
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();;
}

$page="Editar cargo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="cargosave";
    $cargo=fncgetcargo($_GET['id']);
    if ($cargo['profissional']==$_SESSION['id'] or $_SESSION['id']==1){
        //de boa
    }else{
        header("Location: index.php?pg=Vpss_lista");
        exit();;
    }

    if ($cargo['id_pss']==$_GET['pss']){
        //de boa
    }else{
        header("Location: index.php?pg=Vpss_lista");
        exit();;
    }

}else{
    $a="cargonew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vpss&id={$_GET['pss']}&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de Cargos Ofertados no Processo Seletivo</h3>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $cargo['id']; ?>"/>
                <input id="pss" type="hidden" class="txt bradius" name="pss" value="<?php echo $_GET['pss']; ?>"/>
                <input id="pss2" type="hidden" class="txt bradius" name="pss2" value="<?php echo $cargo['id_pss']; ?>"/>
                <label for="nome_cargo">Denominação:</label>
                <input autocomplete="off" id="nome_cargo" placeholder="Cargo/Função" type="text" class="form-control" name="nome_cargo" value="<?php echo $cargo['nome_cargo']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="atribuicao">Atribuição:</label>
                <textarea id="atribuicao" onkeyup="limite_textarea(this.value,1000,atribuicao,'cont1')" maxlength="1000" class="form-control" rows="4" name="atribuicao"><?php echo $cargo['atribuicao']; ?></textarea>
                <span id="cont1">1000</span>/1000
            </div>
            <div class="col-md-6">
                <label for="vagas">Vagas:</label>
                <input id="vagas" type="text" class="form-control" name="vagas" value="<?php echo $cargo['vagas']; ?>"/>
            </div>
            <div class="col-md-6">
                <label for="remuneracao">Remuneração:</label>
                <input id="remuneracao" type="text" class="form-control" name="remuneracao" value="<?php echo $cargo['remuneracao']; ?>"/>
            </div>
            <div class="col-md-6">
                <label for="jornada_semanal">Jornada Semanal:</label>
                <input id="jornada_semanal" type="text" class="form-control" name="jornada_semanal" value="<?php echo $cargo['jornada_semanal']; ?>"/>
            </div>
            <div class="col-md-6">
                <label for="local_lotacao">Local/Lotação::</label>
                <input id="local_lotacao" type="text" class="form-control" name="local_lotacao" value="<?php echo $cargo['local_lotacao']; ?>"/>
            </div>
            <div class="col-md-10">
                <label for="pre_requisito">Pré requisito:</label>
                <textarea id="pre_requisito" onkeyup="limite_textarea(this.value,1000,pre_requisito,'cont2')" maxlength="1000" class="form-control" rows="4" name="pre_requisito"><?php echo $cargo['pre_requisito']; ?></textarea>
                <span id="cont2">1000</span>/1000
            </div>

            <div class="col-md-7">
                <label for="imagem">Imagem de apresentação:</label>**<a href="https://br.freepik.com/fotos-gratis/" target="_blank">ilustre</a>
                <input id="imagem" type="text" class="form-control" name="imagem" value="<?php echo $cargo['imagem']; ?>"/>
            </div>


            <div class="col-md-6">
                <label for="p_anexo">Pedir anexo:</label>
                <select name="p_anexo" id="p_anexo" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_anexo'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_anexo'];
                    } ?>">
                        <?php
                        if ($cargo['p_anexo'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_anexo'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <div class="col-md-6">
                <label for="anexo">Anexo:</label>
                <textarea id="anexo" onkeyup="limite_textarea(this.value,1000,anexo,'cont3')" maxlength="1000" class="form-control" rows="4" name="anexo"><?php echo $cargo['anexo']; ?></textarea>
                <span id="cont3">1000</span>/1000
            </div>

            <div class="col-md-6">
                <label for="p_tempo_funcao">Tempo nessa função:</label>
                <select name="p_tempo_funcao" id="p_tempo_funcao" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_tempo_funcao'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_tempo_funcao'];
                    } ?>">
                        <?php
                        if ($cargo['p_tempo_funcao'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_tempo_funcao'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="p_tempo_outrafuncao">Tempo em outra função no município:</label>
                <select name="p_tempo_outrafuncao" id="p_tempo_outrafuncao" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_tempo_outrafuncao'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_tempo_outrafuncao'];
                    } ?>">
                        <?php
                        if ($cargo['p_tempo_outrafuncao'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_tempo_outrafuncao'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="p_habilitacao">Descrever habilitação na área:</label>
                <select name="p_habilitacao" id="p_habilitacao" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_habilitacao'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_habilitacao'];
                    } ?>">
                        <?php
                        if ($cargo['p_habilitacao'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_habilitacao'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="p_habilitacao">Declarar se tem ensino elementar completo:</label>
                <select name="p_elementar" id="p_elementar" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_elementar'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_elementar'];
                    } ?>">
                        <?php
                        if ($cargo['p_elementar'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_elementar'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="p_medio">Declarar se tem ensino medio completo:</label>
                <select name="p_medio" id="p_medio" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_medio'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_medio'];
                    } ?>">
                        <?php
                        if ($cargo['p_medio'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_medio'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="p_deficiencia">Declarar deficiência:</label>
                <select name="p_deficiencia" id="p_deficiencia" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_deficiencia'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_deficiencia'];
                    } ?>">
                        <?php
                        if ($cargo['p_deficiencia'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_deficiencia'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="p_curso">Curso/cursando:</label>
                <select name="p_curso" id="p_curso" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_curso'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_curso'];
                    } ?>">
                        <?php
                        if ($cargo['p_curso'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_curso'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="p_periodo">Período:</label>
                <select name="p_periodo" id="p_periodo" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_periodo'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_periodo'];
                    } ?>">
                        <?php
                        if ($cargo['p_periodo'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_periodo'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>


            <div class="col-md-6">
                <label for="p_media_global">Media Global:</label>
                <select name="p_media_global" id="p_media_global" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_media_global'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_media_global'];
                    } ?>">
                        <?php
                        if ($cargo['p_media_global'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_media_global'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

<!--           ////////////////////////////////////////////////////////////////////////////////// -->
            <div class="col-md-6 border border-warning">
                <label for="p_pedagogia">Declarar se tem pedagogia:</label>
                <select name="p_pedagogia" id="p_pedagogia" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_pedagogia'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_pedagogia'];
                    } ?>">
                        <?php
                        if ($cargo['p_pedagogia'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_pedagogia'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6 border border-warning">
                <label for="p_pos_graduacao_area">Declarar se tem pos gradução na area:</label>
                <select name="p_pos_graduacao_area" id="p_pos_graduacao_area" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_pos_graduacao_area'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_pos_graduacao_area'];
                    } ?>">
                        <?php
                        if ($cargo['p_pos_graduacao_area'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_pos_graduacao_area'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6 border border-warning">
                <label for="p_mestrado">Declarar se tem mestrado:</label>
                <select name="p_mestrado" id="p_mestrado" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_mestrado'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_mestrado'];
                    } ?>">
                        <?php
                        if ($cargo['p_mestrado'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_mestrado'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6 border border-warning">
                <label for="p_normal_medio_eduinfantil">Declarar se tem CURSO NORMAL DE NÍVEL MÉDIO (ESPECÍFICO PARA EDUCAÇÃO INFANTIL):</label>
                <select name="p_normal_medio_eduinfantil" id="p_normal_medio_eduinfantil" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_normal_medio_eduinfantil'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_normal_medio_eduinfantil'];
                    } ?>">
                        <?php
                        if ($cargo['p_normal_medio_eduinfantil'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_normal_medio_eduinfantil'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6 border border-warning">
                <label for="p_normal_superior">Declarar se tem normal superior:</label>
                <select name="p_normal_superior" id="p_normal_superior" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_normal_superior'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_normal_superior'];
                    } ?>">
                        <?php
                        if ($cargo['p_normal_superior'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_normal_superior'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6 border border-warning">
                <label for="p_magisterio">Declarar se tem magisterio:</label>
                <select name="p_magisterio" id="p_magisterio" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_magisterio'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_magisterio'];
                    } ?>">
                        <?php
                        if ($cargo['p_magisterio'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_magisterio'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-6 border border-warning">
                <label for="p_magisterio2grau">Declarar se tem magisterio a nível de 2º grau:</label>
                <select name="p_magisterio2grau" id="p_magisterio2grau" class="form-control">
                    <option selected="" value="<?php if ($cargo['p_magisterio2grau'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['p_magisterio2grau'];
                    } ?>">
                        <?php
                        if ($cargo['p_magisterio2grau'] == 0) {
                            echo "Não";
                        }
                        if ($cargo['p_magisterio2grau'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

<!--           ////////////////////////////////////////////////////////////////////////////////// -->


            <div class="col-md-6">
                <label for="status">Status:</label>
                <select name="status" id="status" class="form-control">
                    <option selected="" value="<?php if ($cargo['status'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cargo['status'];
                    } ?>">
                        <?php
                        if ($cargo['status'] == 0) {
                            echo "INATIVO";
                        }
                        if ($cargo['status'] ==1) {
                            echo "ATIVO";
                        } ?>
                    </option>
                    <option value="0">INATIVO</option>
                    <option value="1">ATIVO</option>
                </select>
            </div>
            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success btn-block my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>