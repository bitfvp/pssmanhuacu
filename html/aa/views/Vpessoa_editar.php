<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION['nivel_acesso']==1 or $_SESSION['nivel_acesso']==2){

    }else{
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }
}

$page="Sistemas-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

$a="usuariosave2";
$us_er=fncgetusuario($_SESSION['id']);
?>
<main class="container">

    <form class="frmgrid" action="index.php?pg=Vhome&aca=usuariosave" method="post">

        <div class="row">

            <div class="col-md-4">
                <label  class="large" for="nome">Nome:</label>
                <input autocomplete="off" autofocus id="nome" type="text" class="form-control" name="nome" value="<?php echo $us_er['nome']; ?>" required readonly/>
            </div>

            <div class="col-md-3">
                <label  class="large" for="">Nascimento:</label>
                <input name="nascimento" id="nascimento" type="date" class="form-control" value="<?php echo $us_er['nascimento']; ?>" required readonly/>
            </div>

            <div class="col-md-3">
                <label  class="large" for="">CPF:</label>
                <input name="cpf" id="cpf" type="text" class="form-control" value="<?php echo $us_er['cpf']; ?>" required readonly />
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-5">
                <label for="email">E-mail:</label>
                <input autocomplete="off" id="email" type="text" class="form-control" name="email" value="<?php echo $us_er['email']; ?>" required readonly />
            </div>

            <div class="col-md-5">
                <label for="telefone">Telefone:</label>
                <input autocomplete="off" id="telefone" type="tel" class="form-control" name="telefone" value="<?php echo $us_er['telefone']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#telefone').mask('(00)00000-0000', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-5">
                <label for="endereco">Endereço:</label>
                <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $us_er['endereco']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="bairro">Bairro:</label>
                <input autocomplete="off" id="bairro" type="text" class="form-control" name="bairro" value="<?php echo $us_er['bairro']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="cidade">Cidade:</label>
                <input autocomplete="off" id="cidade" type="text" class="form-control" name="cidade" value="<?php echo $us_er['cidade']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="cep">CEP:</label>
                <input autocomplete="off" id="cep" type="text" class="form-control" name="cep" value="<?php echo $us_er['cep']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cep').mask('00000-000', {reverse: false});
                    });
                </script>
            </div>


        </div>

        <div class="row">
            <div class="col">
                <input type="submit" value="SALVAR" class="btn btn-success btn-block mt-3" />
            </div>
        </div>

    </form>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>