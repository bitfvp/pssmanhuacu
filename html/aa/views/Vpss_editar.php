<?php
if(isset($_SESSION['logado']) and $_SESSION['nivel_acesso']==2){
    //acesso liberado
}else{
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();;
}

$page="Editar PS-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="psssave";
    $ps=fncgetpss($_GET['id']);
    if ($ps['profissional']==$_SESSION['id'] or $_SESSION['id']==1){
        //de boa
    }else{
        header("Location: index.php?pg=Vpss_lista");
        exit();;
    }
}else{
    $a="pssnew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vpss_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de Processo Seletivo</h3>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $ps['id']; ?>"/>
                <label for="cod_ps">Código do processo seletivo:</label>
                <input autocomplete="off" id="cod_ps" placeholder="codigo" type="text" class="form-control" name="cod_ps" value="<?php echo $ps['cod_ps']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cod_ps').mask('000/0000', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-5">
                <label for="nome_ps">Denominação:</label>
                <input autocomplete="off" id="nome_ps" placeholder="Processo Seletivo" type="text" class="form-control" name="nome_ps" value="<?php echo $ps['nome_ps']; ?>"/>
            </div>
            <div class="col-md-10">
                <label for="descricao">Descrição:</label>
                <textarea id="descricao" onkeyup="limite_textarea(this.value,3000,descricao,'cont')" maxlength="3000" class="form-control" rows="4" name="descricao"><?php echo $ps['descricao']; ?></textarea>
                <span id="cont">3000</span>/3000
            </div>

            <div class="col-md-5">
                <label for="secretaria">Secretaria:</label>
                <input autocomplete="off" id="secretaria" placeholder="secretaria" type="text" class="form-control" name="secretaria" value="<?php echo $ps['secretaria']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="periodo_insc_i">Inicio das inscriçoes:</label>
                <input id="periodo_insc_i" type="date" class="form-control" name="periodo_insc_i" value="<?php echo $ps['periodo_insc_i']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="periodo_insc_f">Fim das inscriçoes:</label>
                <input id="periodo_insc_f" type="date" class="form-control" name="periodo_insc_f" value="<?php echo $ps['periodo_insc_f']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="data_resultado">Data do resultado:</label>
                <input id="data_resultado" type="date" class="form-control" name="data_resultado" value="<?php echo $ps['data_resultado']; ?>"/>
            </div>
            <div class="col-md-10">
                <label for="local_resultado">Site do resultado:</label>
                <input id="local_resultado" type="text" class="form-control" name="local_resultado" value="<?php echo $ps['local_resultado']; ?>"/>
            </div>

            <div class="col-md-10">
                <label for="edital">Link do edital:</label>
                <input id="edital" type="text" class="form-control" name="edital" value="<?php echo $ps['edital']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="insc_unica">Apenas uma inscrição:</label>
                <select name="insc_unica" id="insc_unica" class="form-control">
                    <option selected="" value="<?php if ($ps['insc_unica'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $ps['insc_unica'];
                    } ?>">
                        <?php
                        if ($ps['insc_unica'] == 0) {
                            echo "DESATIVADO";
                        }
                        if ($ps['insc_unica'] ==1) {
                            echo "ATIVO";
                        }
                        ?>
                    </option>
                    <option value="0">DESATIVADO</option>
                    <option value="1">ATIVO</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="status">Status:</label>
                <select name="status" id="status" class="form-control">
                    <option selected="" value="<?php if ($ps['status'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $ps['status'];
                    } ?>">
                        <?php
                        if ($ps['status'] == 0) {
                            echo "INATIVO";
                        }
                        if ($ps['status'] ==1) {
                            echo "ATIVO";
                        }
                        if ($ps['status'] ==2) {
                            echo "ENCERRADO";
                        }
                        ?>
                    </option>
                    <option value="0">INATIVO</option>
                    <option value="1">ATIVO</option>
                    <option value="2">ENCERRADO</option>
                </select>
            </div>
            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success btn-block my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>