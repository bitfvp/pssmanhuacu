<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark mb-2">

    <nav class='container'>
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="<?php echo $env->env_nome; ?>" style="max-height: 80px">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navinsc" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        PSS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="insc">
                        <a class="dropdown-item" href="index.php?pg=Vpss_lista">LISTA DE PSS</a>
                    </div>
                </li>


                <?php if($_SESSION['nivel_acesso']==1) {?>
                <a class="nav-item nav-link" href="index.php?pg=Vpessoa_editar">DADOS PESSOAIS</a>
                <a class="nav-item nav-link" href="index.php?pg=Vtitulos">TÍTULOS E DOCUMENTOS</a>
                <a class="nav-item nav-link" href="index.php?pg=Vpref">PREFERÊNCIAS DE VAGAS</a>
                <?php }?>

                <?php if($_SESSION['nivel_acesso']==2) {?>
                    <a class="nav-item nav-link" href="index.php?pg=Vpessoa_editar">DADOS PESSOAIS</a>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navinsc" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            USUÁRIOS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="insc">
                            <a class="dropdown-item" href="index.php?pg=Vus">LISTA DE USUÁRIOS</a>
                        </div>
                    </li>

                <?php }?>
            </ul>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">
                        <?php
                        $primeiroNome = explode(" ", $_SESSION["nome"]);
                        echo "Olá ".$primeiroNome[0].", ".Comprimentar();
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                    </a>
                    <div id="qqq" class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a class="dropdown-item" href ="index.php?pg=Vtroca_senha"><i class="fa fa-key"></i>&nbsp;Alterar Senha</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a></li>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <a class="navbar-brand d-none d-xl-block" data-toggle="tooltip" data-trigger="click" data-html="true" title="Grande dia! <i class='fas fa-thumbs-up text-warning'></i>">
        <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/patriaamada.png" alt="<?php echo $env->env__nome; ?>">
    </a>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    <?php
    $_SESSION['nlog']=$_SESSION['nlog']+1;
    if ($_SESSION['nlog']==1){
        ?>
        <script>
            $(function () {
                $('[data-toggle="popover"]').popover();
                $('[data-toggle="popover"]').popover('show');
            })
        </script>
        <?php
    }
    ?>
</nav>