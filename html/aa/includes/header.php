<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

//reports
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/pssmanhuacu/vendor/autoload.php") ;
if ($pathFile) {
$realroot=$_SERVER['DOCUMENT_ROOT']."/pssmanhuacu/";
} else {
$realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();
/*
*   inclui e inicia a classe de configuração de ambiente que é herdada de uma classe abaixo da raiz em models/Env.class.php
*   $env e seus atributos sera usada por todo o sistema
*/
include_once("models/ConfigMod.class.php");
$env=new ConfigMod();

/*
*   inclui e inicia a classe de configuração de drive pdo/mysql
*   $pdo e seus atributos sera usada por todas as querys
*/
include_once("{$env->env_root}models/Db.class.php");////conecção com o db
$pdo = Db::conn();


use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Device;
use Sinergi\BrowserDetector\Language;
$browser = new Browser();
$os = new Os();
$device = new Device();
$language = new Language();


//inclui um controle contendo a funcão que apaga a sessao e desloga
//será chamada da seguinte maneira
//killSession();
include_once("{$env->env_root}controllers/Ks.php");//se ha uma acao

//inclui um controle que ativa uma acao
include_once("{$env->env_root}controllers/action.php");//se ha uma acao

//funcoes diversas
include_once ("{$env->env_root}includes/funcoes.php");//funcoes

//valida o token e gera array
include_once("{$env->env_root}controllers/validaToken.php");

include_once ("{$env->env_root}includes/funcoes_diarias.php");//funcoes diarias

//lista de documento
include_once("{$env->env_root}controllers/documento_lista.php");

//usuario();
include_once("{$env->env_root}models/Usuario.class.php");
include_once("{$env->env_root}controllers/usuario_lista.php");
include_once("{$env->env_root}controllers/usuario_trocasenha.php");
include_once("{$env->env_root}controllers/usuario_edit.php");

//ps
include_once("models/Pss.class.php");
include_once("controllers/pss_novo.php");
include_once("controllers/pss_edicao.php");
include_once("controllers/pss_carregar.php");


//cargo
include_once("models/Cargo.class.php");
include_once("controllers/cargo_novo.php");
include_once("controllers/cargo_edicao.php");
include_once("controllers/cargo_carregar.php");

//Inscricao();
include_once("models/Inscricao.class.php");
include_once("controllers/inscricao_edit.php");
include_once("controllers/inscricao_carregar.php");


//logout
//controle pra logout
include_once("{$env->env_root}controllers/logout.php");

//validação de sessoes ativas
//inclui um controle que valida o hash e renova o mesmo se tiver tudo certo
include_once("{$env->env_root}controllers/validaToken.php");


//metodo de checar usuario, confirma se a session que vincula o login ao hash de sessao no banco de dados esta ativa
include_once("{$env->env_root}controllers/confirmUser.php");