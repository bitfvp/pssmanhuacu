<?php
class Cargo{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccargonew($id_pss,
                                $nome_cargo,
                                $atribuicao,
                                $vagas,
                                $remuneracao,
                                $jornada_semanal,
                                $local_lotacao,
                                $pre_requisito,
                                $imagem,
                                $p_anexo,
                                $anexo,
                                $p_tempo_funcao,
                                $p_tempo_outrafuncao,
                                $p_habilitacao,
                                $p_elementar,
                                $p_medio,
                                $p_deficiencia,
                                $p_curso,
                                $p_periodo,
                                $p_media_global,
                                $p_pedagogia,
                                $p_pos_graduacao_area,
                                $p_mestrado,
                                $p_normal_medio_eduinfantil,
                                $p_normal_superior,
                                $p_magisterio,
                                $p_magisterio2grau,
                                $status,
                                $profissional){

            //inserção no banco
            try{
                $sql="INSERT INTO tbl_pss_cargo ";
                $sql.="(id, id_pss, nome_cargo, atribuicao, vagas, remuneracao, jornada_semanal, local_lotacao, pre_requisito, imagem, p_anexo, anexo, p_tempo_funcao, p_tempo_outrafuncao, p_habilitacao, p_elementar, p_medio, p_deficiencia, p_curso, p_periodo, p_media_global, p_pedagogia, p_pos_graduacao_area, p_mestrado, p_normal_medio_eduinfantil, p_normal_superior, p_magisterio, p_magisterio2grau, status, profissional)";
                $sql.=" VALUES ";
                $sql.="(NULL, :id_pss, :nome_cargo, :atribuicao, :vagas, :remuneracao, :jornada_semanal, :local_lotacao, :pre_requisito, :imagem, :p_anexo, :anexo, :p_tempo_funcao, :p_tempo_outrafuncao, :p_habilitacao, :p_elementar, :p_medio, :p_deficiencia, :p_curso, :p_periodo, :p_media_global, :p_pedagogia, :p_pos_graduacao_area, :p_mestrado, :p_normal_medio_eduinfantil, :p_normal_superior, :p_magisterio, :p_magisterio2grau, :status, :profissional)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":id_pss", $id_pss);
                $insere->bindValue(":nome_cargo", $nome_cargo);
                $insere->bindValue(":atribuicao", $atribuicao);
                $insere->bindValue(":vagas", $vagas);
                $insere->bindValue(":remuneracao", $remuneracao);
                $insere->bindValue(":jornada_semanal", $jornada_semanal);
                $insere->bindValue(":local_lotacao", $local_lotacao);
                $insere->bindValue(":pre_requisito", $pre_requisito);
                $insere->bindValue(":imagem", $imagem);
                $insere->bindValue(":p_anexo", $p_anexo);
                $insere->bindValue(":anexo", $anexo);
                $insere->bindValue(":p_tempo_funcao", $p_tempo_funcao);
                $insere->bindValue(":p_tempo_outrafuncao", $p_tempo_outrafuncao);
                $insere->bindValue(":p_habilitacao", $p_habilitacao);
                $insere->bindValue(":p_elementar", $p_elementar);
                $insere->bindValue(":p_medio", $p_medio);
                $insere->bindValue(":p_deficiencia", $p_deficiencia);
                $insere->bindValue(":p_curso", $p_curso);
                $insere->bindValue(":p_periodo", $p_periodo);
                $insere->bindValue(":p_media_global", $p_media_global);
                $insere->bindValue(":p_pedagogia", $p_pedagogia);
                $insere->bindValue(":p_pos_graduacao_area", $p_pos_graduacao_area);
                $insere->bindValue(":p_mestrado", $p_mestrado);
                $insere->bindValue(":p_normal_medio_eduinfantil", $p_normal_medio_eduinfantil);
                $insere->bindValue(":p_normal_superior", $p_normal_superior);
                $insere->bindValue(":p_magisterio", $p_magisterio);
                $insere->bindValue(":p_magisterio2grau", $p_magisterio2grau);
                $insere->bindValue(":status", $status);
                $insere->bindValue(":profissional", $profissional);
                $insere->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpss&id={$id_pss}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccargoedit($id,
                                 $id_pss,
                                 $nome_cargo,
                                 $atribuicao,
                                 $vagas,
                                 $remuneracao,
                                 $jornada_semanal,
                                 $local_lotacao,
                                 $pre_requisito,
                                 $imagem,
                                 $p_anexo,
                                 $anexo,
                                 $p_tempo_funcao,
                                 $p_tempo_outrafuncao,
                                 $p_habilitacao,
                                 $p_elementar,
                                 $p_medio,
                                 $p_deficiencia,
                                 $p_curso,
                                 $p_periodo,
                                 $p_media_global,
                                 $p_pedagogia,
                                 $p_pos_graduacao_area,
                                 $p_mestrado,
                                 $p_normal_medio_eduinfantil,
                                 $p_normal_superior,
                                 $p_magisterio,
                                 $p_magisterio2grau,
                                 $status){

        //inserção no banco
        try{
            $sql="UPDATE tbl_pss_cargo SET id_pss=:id_pss, nome_cargo=:nome_cargo, atribuicao=:atribuicao, vagas=:vagas, remuneracao=:remuneracao, jornada_semanal=:jornada_semanal, "
                ."local_lotacao=:local_lotacao, pre_requisito=:pre_requisito, imagem=:imagem, p_anexo=:p_anexo, anexo=:anexo, "
                ."p_tempo_funcao=:p_tempo_funcao, p_tempo_outrafuncao=:p_tempo_outrafuncao, p_habilitacao=:p_habilitacao, p_elementar=:p_elementar, p_medio=:p_medio, p_deficiencia=:p_deficiencia, p_curso=:p_curso,p_periodo=:p_periodo,p_media_global=:p_media_global, "
                ."p_pedagogia=:p_pedagogia, p_pos_graduacao_area=:p_pos_graduacao_area, p_mestrado=:p_mestrado, p_normal_medio_eduinfantil=:p_normal_medio_eduinfantil, p_normal_superior=:p_normal_superior, p_magisterio=:p_magisterio, p_magisterio2grau=:p_magisterio2grau, status=:status "
                ."WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":id_pss", $id_pss);
            $insere->bindValue(":nome_cargo", $nome_cargo);
            $insere->bindValue(":atribuicao", $atribuicao);
            $insere->bindValue(":vagas", $vagas);
            $insere->bindValue(":remuneracao", $remuneracao);
            $insere->bindValue(":jornada_semanal", $jornada_semanal);
            $insere->bindValue(":local_lotacao", $local_lotacao);
            $insere->bindValue(":pre_requisito", $pre_requisito);
            $insere->bindValue(":imagem", $imagem);
            $insere->bindValue(":p_anexo", $p_anexo);
            $insere->bindValue(":anexo", $anexo);
            $insere->bindValue(":p_tempo_funcao", $p_tempo_funcao);
            $insere->bindValue(":p_tempo_outrafuncao", $p_tempo_outrafuncao);
            $insere->bindValue(":p_habilitacao", $p_habilitacao);
            $insere->bindValue(":p_elementar", $p_elementar);
            $insere->bindValue(":p_medio", $p_medio);
            $insere->bindValue(":p_deficiencia", $p_deficiencia);
            $insere->bindValue(":p_curso", $p_curso);
            $insere->bindValue(":p_periodo", $p_periodo);
            $insere->bindValue(":p_media_global", $p_media_global);
            $insere->bindValue(":p_pedagogia", $p_pedagogia);
            $insere->bindValue(":p_pos_graduacao_area", $p_pos_graduacao_area);
            $insere->bindValue(":p_mestrado", $p_mestrado);
            $insere->bindValue(":p_normal_medio_eduinfantil", $p_normal_medio_eduinfantil);
            $insere->bindValue(":p_normal_superior", $p_normal_superior);
            $insere->bindValue(":p_magisterio", $p_magisterio);
            $insere->bindValue(":p_magisterio2grau", $p_magisterio2grau);
            $insere->bindValue(":status", $status);
            $insere->bindValue(":id", $id);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpss&id={$id_pss}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}