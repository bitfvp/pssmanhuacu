<?php
class Inscricao{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncinscricao_verifica($id,
                                          $v_cpf,
                                          $v_rg,
                                          $v_certificado,
                                          $v_curriculo,
                                          $v_esperiencia,
                                          $v_tfm,
                                          $v_tofm,
                                          $v_cursos,
                                          $v_especializacao,
                                          $v_apto,
                                          $v_tp
    ){

        //inserção no banco
        try{
            $sql="UPDATE tbl_pss_inscricao SET v_cpf=:v_cpf, v_rg=:v_rg, v_certificado=:v_certificado, v_curriculo=:v_curriculo, "
                ."v_esperiencia=:v_esperiencia, v_tfm=:v_tfm, v_tofm=:v_tofm, v_cursos=:v_cursos, v_especializacao=:v_especializacao, v_apto=:v_apto, v_tp=:v_tp, v_data = CURRENT_TIMESTAMP, verificado=1 "
                ."WHERE id=:id ";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":v_cpf", $v_cpf);
            $insere->bindValue(":v_rg", $v_rg);
            $insere->bindValue(":v_certificado", $v_certificado);
            $insere->bindValue(":v_curriculo", $v_curriculo);
            $insere->bindValue(":v_esperiencia", $v_esperiencia);
            $insere->bindValue(":v_tfm", $v_tfm);
            $insere->bindValue(":v_tofm", $v_tofm);
            $insere->bindValue(":v_cursos", $v_cursos);
            $insere->bindValue(":v_especializacao", $v_especializacao);
            $insere->bindValue(":v_apto", $v_apto);
            $insere->bindValue(":v_tp", $v_tp);
            $insere->bindValue(":id", $id);

            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }




    //fim da class
}