<?php
class PsS{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpssnew($profisional,$cod_ps,$nome_ps,$descricao,$secretaria,$periodo_insc_i,$periodo_insc_f,$data_resultado,$local_resultado,$edital,$insc_unica,$status){

            //inserção no banco
            try{
                $sql="INSERT INTO tbl_pss ";
                $sql.="(id, profissional, cod_ps, nome_ps, descricao, secretaria, periodo_insc_i, periodo_insc_f, data_resultado, local_resultado, insc_unica, status, edital)";
                $sql.=" VALUES ";
                $sql.="(NULL, :profissional, :cod_ps, :nome_ps, :descricao, :secretaria, :periodo_insc_i, :periodo_insc_f, :data_resultado, :local_resultado, :insc_unica, :status, :edital)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":profissional", $profisional);
                $insere->bindValue(":cod_ps", $cod_ps);
                $insere->bindValue(":nome_ps", $nome_ps);
                $insere->bindValue(":descricao", $descricao);
                $insere->bindValue(":secretaria", $secretaria);
                $insere->bindValue(":periodo_insc_i", $periodo_insc_i);
                $insere->bindValue(":periodo_insc_f", $periodo_insc_f);
                $insere->bindValue(":data_resultado", $data_resultado);
                $insere->bindValue(":local_resultado", $local_resultado);
                $insere->bindValue(":insc_unica", $insc_unica);
                $insere->bindValue(":status", $status);
                $insere->bindValue(":edital", $edital);
                $insere->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpss_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpssedit($id,$cod_ps,$nome_ps,$descricao,$secretaria,$periodo_insc_i,$periodo_insc_f,$data_resultado,$local_resultado,$edital,$insc_unica,$status){

        //inserção no banco
        try{
            $sql="UPDATE tbl_pss SET cod_ps=:cod_ps, nome_ps=:nome_ps, descricao=:descricao, secretaria=:secretaria, "
                ."periodo_insc_i=:periodo_insc_i, periodo_insc_f=:periodo_insc_f, data_resultado=:data_resultado, local_resultado=:local_resultado, edital=:edital, insc_unica=:insc_unica, status=:status "
                ."WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":cod_ps", $cod_ps);
            $insere->bindValue(":nome_ps", $nome_ps);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":secretaria", $secretaria);
            $insere->bindValue(":periodo_insc_i", $periodo_insc_i);
            $insere->bindValue(":periodo_insc_f", $periodo_insc_f);
            $insere->bindValue(":data_resultado", $data_resultado);
            $insere->bindValue(":local_resultado", $local_resultado);
            $insere->bindValue(":edital", $edital);
            $insere->bindValue(":insc_unica", $insc_unica);
            $insere->bindValue(":status", $status);
            $insere->bindValue(":id", $id);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpss&id={$id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}