<?php
function fncgetinscricao($id){
    $sql = "SELECT * FROM tbl_pss_inscricao WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getcargo = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcargo;
}