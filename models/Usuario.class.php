<?php 
class Usuario{
	public function fncnewusuario(
        $status,
        $nivel_acesso,
        $nome,
        $cpf,
        $nascimento,
        $email,
        $senha,
        $dispositivo,
        $sistema_operacional,
        $navegador,
        $senhatest
    ){
		//tratamento das variaveis

        if($nascimento==""){
            $nascimento="1900-01-01";
        }
		//valida se ja ha um usuario cadastrado
        try{
            $sql="SELECT * FROM tbl_users WHERE cpf=:cpf";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":cpf", $cpf);
            $consulta->execute();

        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

		$contar=$consulta->rowCount();
		if($contar==0){
			//inserção no banco
            try{
                $sql="INSERT INTO tbl_users("
                    ."status, nivel_acesso, nome, cpf, nascimento, email, senha"
                    .")VALUES("
                    .":status, :nivel_acesso, :nome, :cpf, :nascimento, :email, :senha"
                    .")";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":status", $status);
                $insere->bindValue(":nivel_acesso", $nivel_acesso);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":cpf", $cpf);
                $insere->bindValue(":nascimento", $nascimento);
                $insere->bindValue(":email", $email);
                $insere->bindValue(":senha", $senha);
                $insere->execute();

            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

		}else{
			//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já há um usuário cadastrado com esse CPF em nosso sistema!!!!",
                "type"=>"danger",
            ];

		}

			if(isset($insere)){


                $login = new Login;
                $login = $login->fnclogar($cpf, $senha, $dispositivo, $sistema_operacional, $navegador, $senhatest);

                //cria login
                
			}else{
				if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                    $_SESSION['fsh']=[
                        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                        "type"=>"danger",
                    ];
				}
			}
	}

//=========================================================================================
///
//=========================================================================================
    public function fncusuarioedit(
        $pessoa,
        $telefone,
        $endereco,
        $bairro,
        $cidade,
        $cep
    )
    {
        try{
            $sql = "UPDATE tbl_users SET "
                ."telefone = :telefone, "
                ."endereco = :endereco, "
                ."bairro = :bairro, "
                ."cidade = :cidade, "
                ."cep = :cep "
                ."WHERE id = :id";
            global $pdo;
            $insere2=$pdo->prepare($sql);
            $insere2->bindValue(":telefone", $telefone);
            $insere2->bindValue(":endereco", $endereco);
            $insere2->bindValue(":bairro", $bairro);
            $insere2->bindValue(":cidade", $cidade);
            $insere2->bindValue(":cep", $cep);
            $insere2->bindValue(":id", $pessoa);
            $insere2->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }
        

        if(isset($insere2)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }
/// //==================================================================================================

//=========================================================================================
    public function fncnewsenha($id,$senha){
        //tratamento das variaveis
        $senha=sha1($senha."1010011010");

        //atualização no banco
        try{
            $sql="UPDATE tbl_users SET senha=:senha ";
            $sql.="WHERE id=:id";
            global $pdo;
            $at=$pdo->prepare($sql);
            $at->bindValue(":id", $id);
            $at->bindValue(":senha", $senha);
            $at->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if(isset($at)){
            $_SESSION['fsh']=[
                "flash"=>"Atualização de senha realizado com sucesso!!",
                "type"=>"success",
                "error"=>"No proximo login use a nova senha cadastrada",
            ];
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
//==================================================================================================
//==================================================================================================
//==================================================================================================
//==================================================================================================


    public function fncresetsenha($id){
        //inserção no banco
        $ns1=rand(0, 9);
        $ns2=rand(0, 9);
        $ns3=rand(0, 9);
        $ns4=rand(0, 9);
        $ns5=rand(0, 9);
        $ns6=rand(0, 9);
        $ns7=rand(0, 9);
        $ns8=rand(0, 9);
        $ns=$ns1.$ns2.$ns3.$ns4.$ns5.$ns6.$ns7.$ns8;
        $ns_sha=sha1($ns."1010011010");
        try{
            $sql = "UPDATE tbl_users SET senha = :novasenha WHERE id = :id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":novasenha", $ns_sha);
            $insere->bindValue(":id", $id);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Reset de senha feito com sucesso!! ",
                "type"=>"success",
                "error"=>"<h2>Nova senha: {$ns}</h2>"
            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador ",
                    "type"=>"danger",
                ];

            }
        }
    }


//==================================================================================================
//==================================================================================================
//==================================================================================================
//==================================================================================================






}