<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="usuarionew"){
    if ($securimage->check($_POST['captcha_code']) == false) {
        $_SESSION['fsh']=[
            "flash"=>"Digite corretamente os caracteres da imagem.",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vreg");
        exit();
    }else{
        if ($_POST["senha1"]==$_POST["senha2"]){
            $status = '1';
            $nivel_acesso = '1';
            $nome = remover_caracter(ucwords(strtolower($_POST["nome"])));
            $cpf = limpadocumento($_POST["cpf"]);
            $nascimento = $_POST["nascimento"];
            if($nascimento==""){
                $nascimento="1000-01-01";
            }
            $email = strtolower($_POST["email"]);
            $senha=sha1($_POST["senha1"]."1010011010");
            $senhatest=$_POST["senha1"];


            if(empty($nome) || empty($email) || empty($nascimento) || $nascimento==0){
                $_SESSION['fsh']=[
                    "flash"=>"Preencha todos os campos!!",
                    "type"=>"warning",
                ];
            }else{
                $dispositivo = $device->getName()." ".$_SERVER['REMOTE_ADDR'];
                $sistema_operacional = $os->getName()." ".$os->getVersion();
                if ($os->isMobile()==true){
                    $sistema_operacional .=" Mobile";
                }
                $navegador = $browser->getName()." ".$browser->getVersion();

                //executa classe cadastro
                $conec= new Usuario();
                $conec=$conec->fncnewusuario(
                    $status,
                    $nivel_acesso,
                    $nome,
                    $cpf,
                    $nascimento,
                    $email,
                    $senha,
                    $dispositivo,
                    $sistema_operacional,
                    $navegador,
                    $senhatest
                );

            }
        }else{
            $_SESSION['fsh']=[
                "flash"=>"As duas senhas não são igual, tente novamente.",
                "type"=>"warning",
            ];
            header("Location: index.php?pg=Vreg");
            exit();
        }


    }

}
