<?php

function fncdocumentolist(){
    $sql = "SELECT * FROM tbl_pss_inscricao_lista_doc ORDER BY documento ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $documentolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $documentolista;
}

function fncgetdocumento($id){
    $sql = "SELECT * FROM tbl_pss_inscricao_lista_doc WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getdocumento = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getdocumento;
}
