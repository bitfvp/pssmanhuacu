<?php

//metodo de recuperar senha
if ($startactiona == 1 && $aca == "recuperar_senha") {

    if ($securimage->check($_POST['captcha_code']) == false) {
        $_SESSION['fsh']=[
            "flash"=>"Digite corretamente os caracteres da imagem.",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vrec");
        exit();
    }else{

        //verifica se ja nao esta logado
        if(isset($_SESSION['logado'])){
            header("Location: {$env->env_url}a");
            exit();
        }else{
            //verificar se as post estao setadas
            $cpf = limpadocumento(addslashes($_POST["syscpf"]));
            $email = $_POST["sysemail"];
            //verifica se o banco tem o usuario e senha
            try{
                $sql="select * from tbl_users WHERE cpf=? AND email=? limit 1";
                global $pdo;
                $consulta=$pdo->prepare($sql);
                $consulta->bindParam(1, $cpf);
                $consulta->bindParam(2, $email);
                $consulta->execute();
                $cadcount = $consulta->rowCount();
                $cad = $consulta->fetch();
                $sql=null;
                $consulta=null;
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }
            //verifica se existe >0 registros
            if( $cadcount!=0){

                //verifica se não foi enviado o email já
                try{
                    $sql="select * from tbl_recupera_senha WHERE user=? ORDER BY id desc limit 1";
                    global $pdo;
                    $consulta=$pdo->prepare($sql);
                    $consulta->bindParam(1, $cad['id']);
                    $consulta->execute();
                    $reccount = $consulta->rowCount();
                    $rec = $consulta->fetch();
                    $sql=null;
                    $consulta=null;
                }catch ( PDOException $error_msg){
                    echo 'Erroff'. $error_msg->getMessage();
                }
                //tempo agora
                $mknow = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                //tempo mais uma hora
                $newmk = $mknow + 3600;

                if ($reccount!=0 and $rec['token_time']>$mknow){
                    $_SESSION['fsh']=[
                        "flash"=>"Não foi possível :(",
                        "type"=>"warning",
                    ];
                    $_SESSION['recupera_email']=$email;
                    header("Location: index.php?pg=Vrec");
                    exit();
                }

                $ns1=rand(0, 9);
                $ns2=rand(0, 9);
                $ns3=rand(0, 9);
                $ns4=rand(0, 9);
                $ns5=rand(0, 9);
                $ns6=rand(0, 9);
                $ns7=rand(0, 9);
                $ns8=rand(0, 9);
                $ns=$ns1.$ns2.$ns3.$ns4.$ns5.$ns6.$ns7.$ns8;
                $ns_sha=sha1($ns."1010011010");
                try{
                    $sql = "UPDATE tbl_users SET senha_recupera = :novasenha WHERE cpf=:cpf AND email=:email";

                    global $pdo;
                    $insere=$pdo->prepare($sql);
                    $insere->bindValue(":novasenha", $ns_sha);
                    $insere->bindValue(":cpf", $cpf);
                    $insere->bindValue(":email", $email);
                    $insere->execute();
                }catch ( PDOException $error_msg){
                    echo 'Erro'. $error_msg->getMessage();
                }


                ///
                $m = new SimpleEmailServiceMessage();
                $m->addTo($email);
                $m->setFrom('pssmanhuacu@gmail.com');
                $tema='Redefinição de senha';
                $m->setSubject($tema);
                $corpo="Olá ".$cad['nome'].", <br>"
                    ."Foi solicitada uma redefinição da sua senha no pssmanhuacu.com.br, para acessar o site use: <br>"
                    ."<h3>CPF:".$cpf."   SENHA:".$ns."</h3>"
                    ."<br>"
                    ."Caso você não tenha solicitado essa redefinição, ignore esta mensagem.<br>"
                    ."Qualquer problema ou dúvida entre em contato pelo e-mail pssmanhuacu@gmail.com<BR>"
                    ."<i>Este é um e-mail automático, por favor, não responda.</i>";
                $m->setMessageFromString($corpo,$corpo);

                $ses = new SimpleEmailService('AKIA2MSUBDNKMXV6AQWO', 'E6Ufo7TEDjnq3LII6wUC4vB48bl8D0bAUo4GVnqb');
                print_r($ses->sendEmail($m));
//

                //salvar se foi recuperado senha
                $dispositivo = $device->getName()." ".$_SERVER['REMOTE_ADDR'];
                $sistema_operacional = $os->getName()." ".$os->getVersion();
                if ($os->isMobile()==true){
                    $sistema_operacional .=" Mobile";
                }
                $navegador = $browser->getName()." ".$browser->getVersion();
                $sql = "insert into tbl_recupera_senha ";
                $sql .= "(id, nova_senha, token_time, user, dispositivo, sistema_operacional, navegador) values(NULL, :nova_senha, :time, :user, :dispositivo, :sistema_operacional, :navegador) ";
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":nova_senha", $ns);
                $insere->bindValue(":time", $newmk);
                $insere->bindValue(":dispositivo", $dispositivo);
                $insere->bindValue(":sistema_operacional", $sistema_operacional);
                $insere->bindValue(":navegador", $navegador);
                $insere->bindValue(":user", $cad['id']);
                $insere->execute();
                $sql = null;
                $consulta = null;

                $_SESSION['fsh']=[
                    "flash"=>"As orientações para acessar com sua conta foram enviadas ao seu e-mail.",
                    "type"=>"info",
                ];
                $_SESSION['recupera_email']=$email;

                header("Location: index.php?pg=Vrec");
                exit();

///
            }else{
                $_SESSION['fsh']=[
                    "flash"=>"Não há cadastro no sistema com essa combinação de E-mail e CPF",
                    "type"=>"warning",
                ];
                header("Location: index.php?pg=Vrec");
                exit();
            }

        }
    }





}//fim de controle